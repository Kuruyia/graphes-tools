namespace embedEditMod {
    function tryToParseHexColor(hexColor: string): KwimMod.Vec3D
    {
        let parsedColor: KwimMod.Vec3D = {x: 0, y: 0, z: 0};

        if (hexColor.length === 7 && hexColor.startsWith('#'))
        {
            const r: number = parseInt(hexColor.slice(1, 3), 16);
            const g: number = parseInt(hexColor.slice(3, 5), 16);
            const b: number = parseInt(hexColor.slice(5, 7), 16);

            if (!isNaN(r) && !isNaN(g) && !isNaN(b))
                parsedColor = {x: r, y: g, z: b};
        }

        return parsedColor;
    }

    function rgbColorToString(rgb: KwimMod.Vec3D): string
    {
        function numToComponent(n: number): string
        {
            let str: string = Math.floor(n * 255).toString(16);
            if (str.length === 1)
                str = '0' + str;

            return str;
        }

        return '#'.concat(numToComponent(rgb.x), numToComponent(rgb.y), numToComponent(rgb.z));
    }

    export function doubleClickHandler(this: any, e: MouseEvent)
    {
        // Check if we're in the right mode
        if (mode == 0)
        {
            // Look for a node under the mouse cursor
            const x: number = e.offsetX;
            const y: number = e.offsetY;
            const selectedNode: Vertex | null = g.findVertexByPos({x: x, y: y});

            if (selectedNode != null)
            {
                // Ungrab the node
                grabbed = null;

                // Prepare the edit window
                const windowName: string = 'vertexEdit' + selectedNode.nom;
                let editWindow: KwimMod.Window = new KwimMod.Window(windowName, 'Edit node ' + selectedNode.nom,
                    {x: selectedNode.position.x + vertexSize, y: selectedNode.position.y + vertexSize}, {x: 288, y: 282});

                editWindow.addChild(new KwimMod.Text('nameText', 'Name', {x: 8, y: 8}));

                // Textbox to edit the node name
                let nameTxtbox: KwimMod.TextBox = new KwimMod.TextBox('nameTxtbox', {x: 8, y: 30}, {x: 272, y: 24});
                editWindow.addChild(nameTxtbox);
                
                nameTxtbox.setText(selectedNode.nom);

                // Color picker to pick a background color
                let bgColorText = new KwimMod.Text('bgColorText', 'Background color', {x: 72, y: 80});
                editWindow.addChild(bgColorText);
                bgColorText.setHorizontalAlignment('center');

                let bgColorPicker: KwimMod.ColorPicker = new KwimMod.ColorPicker('bgColorPicker', {x: 8, y: 104}, 64);
                editWindow.addChild(bgColorPicker);

                const parsedBgColor: KwimMod.Vec3D = tryToParseHexColor(selectedNode.fillStyle);
                bgColorPicker.setRgbColor(parsedBgColor.x / 255, parsedBgColor.y / 255, parsedBgColor.z / 255);

                // Color picker to pick a line color
                let textColorText = new KwimMod.Text('textColorText', 'Text color', {x: 216, y: 80});
                editWindow.addChild(textColorText);
                textColorText.setHorizontalAlignment('center');

                let lineColorPicker: KwimMod.ColorPicker = new KwimMod.ColorPicker('lineColorPicker', {x: 152, y: 104}, 64);
                editWindow.addChild(lineColorPicker);

                const parsedLineColor: KwimMod.Vec3D = tryToParseHexColor(selectedNode.strokeStyle);
                lineColorPicker.setRgbColor(parsedLineColor.x / 255, parsedLineColor.y / 255, parsedLineColor.z / 255);

                // Setup the dialog buttons
                let okButton: KwimMod.Button = new KwimMod.Button('okButton', 'OK', {x: 200, y: 250}, {x: 80, y: 24});
                editWindow.addChild(okButton);

                okButton.addClickHandler(() => {
                    selectedNode.nom = nameTxtbox.getText();

                    selectedNode.fillStyle = rgbColorToString(bgColorPicker.getColorAsRgb());
                    selectedNode.strokeStyle = rgbColorToString(lineColorPicker.getColorAsRgb());

                    KwimMod.closeWindow(windowName);
                });

                let cancelButton: KwimMod.Button = new KwimMod.Button('cancelButton', 'Cancel', {x: 112, y: 250}, {x: 80, y: 24});
                editWindow.addChild(cancelButton);

                cancelButton.addClickHandler(() => {
                    KwimMod.closeWindow(windowName);
                });
                    
                KwimMod.addWindow(editWindow);
            }
        }
    }
}

// Totally hijack the default interface
cv.addEventListener('dblclick', embedEditMod.doubleClickHandler);
vertexInterface?.remove();