let modsDiv = <HTMLElement>document.getElementById("mods")
let interfaceOutput = document.getElementById("interface-output")
let interfaceInput = document.getElementById("interface-input")

let interfaceOutputContent = <HTMLElement>document.getElementById("interface-output-content")
let interfaceOutputBackButton = <HTMLButtonElement>document.getElementById("output-button-back")
let interfaceInputCancelButton = <HTMLButtonElement>document.getElementById("input-button-cancel")
let interfaceInputApplyButton = <HTMLButtonElement>document.getElementById("input-button-apply")
let interfaceInputContent = <HTMLElement>document.getElementById("interface-input-content")
let interfeceOutputContent = document.getElementById("interface-output-content")

let interfaceInputTitle = <HTMLElement>document.getElementById("interface-input-title")
let modsButton = <HTMLButtonElement>document.getElementById("modsSelect")



class Mod {
    callback: (g: Graph) => void
    name: string
    constructor(callback: (g: Graph) => void, name: string) {
        this.callback = callback
        this.name = name
    }

    run() {

        this.callback(g)
    }
}


let mods: Array<Mod> = []


function addModRunner(runner: (g: Graph) => void, name: string) {
    let m = new Mod(runner, name)
    mods.push(m)
}
function addModDisplay(displayRunner: (context: CanvasRenderingContext2D) => any) {
    refreshCallbacks.push(displayRunner)
}





function importModule(this: HTMLInputElement, e: Event) {
    if (this.files) {
        if (this.files.length > 0) {
            let file = this.files[0]
            let scriptEl = document.createElement("script")


            let reader = new FileReader()
            reader.addEventListener("load", (e) => {
                if (e.target?.result) {
                    // eval(e.target.result.toString())
                    scriptEl.innerHTML = e.target?.result.toString()
                    modsDiv?.appendChild(scriptEl)
                    setTimeout(menuMods, 500)
                }
            })
            reader.readAsText(file)
        }

    }


}


function menuMods() {

    let it = document.createElement("div")
    for (let i = 0; i < mods.length; ++i) {
        let p = document.createElement("p")
        p.style.paddingBottom = "10px"
        p.innerText = mods[i].name + " : "
        let runButton = document.createElement("button")
        runButton.addEventListener("click", () => {
            mods[i].run()
            refresh()
        })
        runButton.innerText = "run"
        p.appendChild(runButton)
        it.appendChild(p)
    }

    let importScriptButton = document.createElement("input")
    importScriptButton.id = "importScriptButton"
    importScriptButton.type = "file"
    importScriptButton.addEventListener("change", importModule)
    let labelScriptButton = document.createElement("label")
    labelScriptButton.htmlFor = "importScriptButton"
    labelScriptButton.innerText = "import a script : "
    it.appendChild(labelScriptButton)
    it.appendChild(importScriptButton)
    interfaceOutputHTML(it);
}


modsButton.addEventListener("click", menuMods)