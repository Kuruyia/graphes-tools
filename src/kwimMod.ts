/**
 * Kwim - Kuruyia's Widget and Window Manager
 * 
 * MIT License
 * 
 * Copyright (c) 2020 Alexandre "Kuruyia" SOLLIER
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**
 * Namespace holding class and methods used to work
 * with the Kwim mod.
 */
namespace KwimMod {
    /**
     * An interface used to hold coordinates in a 2D space.
     */
    export interface Vec2D
    {
        /** The X coordinate */
        x: number,
        
        /** The Y coordinate */
        y: number
    }

    /**
     * An interface used to hold coordinates in a 3D space.
     */
    export interface Vec3D
    {
        /** The X coordinate */
        x: number,
        
        /** The Y coordinate */
        y: number,
        
        /** The Z coordinate */
        z: number
    }

    /**
     * Utility class to manage event handlers.
     */
    class EventHandlerManager
    {
        /** Holds the event handlers for an instance of {@link EventHandlerManager} */
        private m_handlers: Array<Function>;

        /** Constructor for {@link EventHandlerManager} */
        constructor()
        {
            this.m_handlers = new Array<Function>();
        }

        /** 
         * Adds an event handler to the manager.
         * @param f Event handler to add
         */
        addHandler(f: Function) { this.m_handlers.push(f); }

        /**
         * Removes an event handler from the manager.
         * @param f Event handler to remove
         */
        removeHandler(f: Function): void
        {
            // Loop through all the handlers and remove the requested one if found
            let i = 0;
            while (i < this.m_handlers.length)
            {
                if (this.m_handlers[i] === f)
                {
                    this.m_handlers.splice(i, 1);
                    return;
                }

                ++i;
            }
        }

        /**
         * Calls every registered handler with the provided arguments.
         * @param args Arguments forwaded to the handlers
         */
        callHandlers(...args: any[]): void
        {
            // Call all the handlers with arguments
            for (let handler of this.m_handlers)
                handler.apply(null, arguments);
        }
    }

    /**
     * Base class representing any widget.
     */
    export abstract class Widget
    {
        /** Flag representing if the widget has finished setup */
        protected m_ready: boolean;

        /** ID of the widget */
        protected m_id: string;

        /** 
         * Position of the widget
         * 
         * This position is relative to the parent if any,
         * or absolute to the canvas if none.
         */
        protected m_position: Vec2D;

        /** Parent of the widget */
        protected m_parent?: Widget;

        /** Children of the widget */
        protected m_children: Array<Widget>;

        /**
         * Constructs the base of a widget.
         * @param id ID of the widget
         * @param position Position of the widget
         */
        constructor(id: string, position: Vec2D)
        {
            this.m_ready = false;
            this.m_id = id;
            this.m_position = position;

            this.m_children = [];
        }

        /**
         * Adds a child to this widget.
         * 
         * *Note*: For performance reasons, 
         * it is important to **directly** add a child to its parent after having constructed it and before setting it up.
         * 
         * Example:
         * ```TypeScript
         * let win: KwimMod.Window = new KwimMod.Window('win', 'A Window', {x: 8, y: 8}, {x: 256, y: 256});
         * let btn: KwimMod.Button = new KwimMod.Button('btn', 'Button', {x: 0, y: 0}, {x: 128, y: 24});
         * win.addChild(btn); // We add the Button to the Window immediately
         *
         * btn.addClickHandler((target: KwimMod.Button) => {
         *     console.log('Button clicked!');
         * });
         * ```
         * @param widget The widget to add
         */
        addChild(widget: Widget): void
        {
            // Add the widget to the children array
            widget.setParent(this);
            this.m_children.push(widget);
        }

        /**
         * Searches for a widget with the given ID,
         * and return it if found.
         * @param id ID of the widget to look for
         * @returns The widget, if found
         */
        findChildById(id: string): Widget | undefined
        {
            // Try to find the widget and return it if found
            for (let child of this.m_children)
                if (child.getId() === id)
                    return child;

            return undefined;
        }

        /**
         * Renders the widget using the provided context.
         * @param ctx Rendering context
         */
        draw(ctx: CanvasRenderingContext2D): void
        {
            if (!this.m_ready) return;

            // Draw the children
            for (let child of this.m_children)
                child.draw(ctx);
        }

        /**
         * Processes an incoming event.
         * @param e Event to process
         * @param data Additionnal data
         */
        processEvent(e: Event, data: any): void
        {
            if (!this.m_ready) return;

            // Send event to the children
            for (let child of this.m_children)
                child.processEvent(e, data);
        }

        /**
         * Sends a request to refresh the scene,
         * if this widget is ready.
         */
        refresh(): void
        {
            // Get to the root parent, and ask it to refresh if ready
            if (this.m_parent !== undefined)
                this.m_parent.refresh();
            else if (this.m_ready)
                requestRefresh();
        }

        /**
         * Gets the ID of the widget.
         * @returns ID of the widget
         */
        getId(): string { return this.m_id; }

        /**
         * Sets the position of the widget.
         * @param position New position of the widget
         */
        setPosition(position: Vec2D): void { this.m_position = position; this.refresh(); }

        /**
         * Gets the position of the widget.
         * @returns Position of the widget.
         */
        getPosition(): Vec2D { return this.m_position; }

        /**
         * Computes the absolute position of the widget
         * in the canvas.
         * @returns The absolute position of the widget
         */
        getAbsolutePosition(): Vec2D
        {
            if (this.m_parent === undefined)
                return this.m_position;

            const parentAbsolutePosition: Vec2D = this.m_parent.getAbsolutePosition();
            return {x: parentAbsolutePosition.x + this.m_position.x, y: parentAbsolutePosition.y + this.m_position.y};
        }

        /**
         * Sets the parent of the widget.
         * @param parent The new parent
         */
        setParent(parent?: Widget): void { this.m_parent = parent; this.refresh(); }

        /**
         * Gets the parent of the widget.
         * @returns The parent of the widget, if any
         */
        getParent(): Widget | undefined { return this.m_parent; }

        /**
         * Utility method to draw a rounded rectangle.
         * @param ctx Rendering context
         * @param position Absolute position of the top-left corner of the rectangle
         * @param size Size of the rectangle
         * @param cornerRadius Radius for the 4 corners
         */
        protected static drawRoundedRectangle(ctx: CanvasRenderingContext2D, position: Vec2D, size: Vec2D, cornerRadius: number): void
        {
            ctx.beginPath();
            ctx.moveTo(position.x + cornerRadius, position.y);
            ctx.lineTo(position.x + size.x - cornerRadius, position.y);
            ctx.quadraticCurveTo(position.x + size.x, position.y, position.x + size.x, position.y + cornerRadius);
            ctx.lineTo(position.x + size.x, position.y + size.y - cornerRadius);
            ctx.quadraticCurveTo(position.x + size.x, position.y + size.y, position.x + size.x - cornerRadius, position.y + size.y);
            ctx.lineTo(position.x + cornerRadius, position.y + size.y);
            ctx.quadraticCurveTo(position.x, position.y + size.y, position.x, position.y + size.y - cornerRadius);
            ctx.lineTo(position.x, position.y + cornerRadius);
            ctx.quadraticCurveTo(position.x, position.y, position.x + cornerRadius, position.y);
            ctx.closePath();
            ctx.fill();
        }

        /**
         * Utility method to draw a circle.
         * @param ctx Rendering context
         * @param center Absolute position of the circle center
         * @param radius Radius of the circle
         */
        protected static drawCircle(ctx: CanvasRenderingContext2D, center: Vec2D, radius: number)
        {
            ctx.beginPath();
            ctx.arc(center.x, center.y, radius, 0, Math.PI * 2);
            ctx.closePath();
            ctx.fill();
        }
    }

    /**
     * Widget whose only purpose is to contain other widgets.
     */
    export class Container extends Widget
    {
        /** Size of the container */
        private m_size: Vec2D;

        /**
         * Constructs the container.
         * @param id ID of the widget
         * @param position Position of the widget
         * @param size Size of the widget
         */
        constructor(id: string, position: Vec2D, size: Vec2D)
        {
            super(id, position);

            this.m_size = size;

            this.m_ready = true;
        }

        /**
         * Renders the container background, and its children.
         * @param ctx Rendering context
         */
        draw(ctx: CanvasRenderingContext2D): void
        {
            const drawPosition = this.getAbsolutePosition();

            // Draw the background of the window
            ctx.fillStyle = '#3D3D3D';
            ctx.fillRect(drawPosition.x, drawPosition.y, this.m_size.x, this.m_size.y);

            super.draw(ctx);
        }
    }

    /** Enumerates the different events from {@link WindowButtons}. */
    export enum WindowButtonEvent
    {
        /** The close button has been clicked */
        BUTTON_CLOSE
    }

    /**
     * Class containing the buttons for a {@link Window}.
     */
    export class WindowButtons extends Widget
    {
        /** Flag indicating if the window buttons has focus */
        private m_focused: boolean;

        /** Event manager for button events */
        private m_buttonEventMgr: EventHandlerManager;

        /** Radius of the close button */
        private static readonly s_closeRadius: number = 8;

        /** Margin of the cross inside the close button */
        private static readonly s_closeLineDistance: number = WindowButtons.s_closeRadius * (Math.sqrt(2) / 2) - 2;

        /**
         * Constructs some buttons for a {@link Window}.
         * @param id ID of the widget
         * @param position Position of the widget
         */
        constructor(id: string, position: Vec2D)
        {
            super(id, position);

            this.m_focused = false;

            this.m_buttonEventMgr = new EventHandlerManager();

            this.m_ready = true;
        }

        /**
         * Renders the buttons using the provided context.
         * @param ctx Rendering context
         */
        draw(ctx: CanvasRenderingContext2D)
        {
            const drawPosition: Vec2D = this.getAbsolutePosition();
            const closeCenter: Vec2D = {x: drawPosition.x + WindowButtons.s_closeRadius, y: drawPosition.y + WindowButtons.s_closeRadius};

            // Prepare draw parameters
            ctx.fillStyle = '#FF5722';
            ctx.strokeStyle = 'white';
            ctx.lineWidth = 1;

            // Draw the close button
            Widget.drawCircle(ctx, closeCenter, WindowButtons.s_closeRadius);

            // Draw the close cross
            ctx.beginPath();
            ctx.moveTo(closeCenter.x - WindowButtons.s_closeLineDistance, closeCenter.y + WindowButtons.s_closeLineDistance);
            ctx.lineTo(closeCenter.x + WindowButtons.s_closeLineDistance, closeCenter.y - WindowButtons.s_closeLineDistance);
            ctx.stroke();

            ctx.beginPath();
            ctx.moveTo(closeCenter.x - WindowButtons.s_closeLineDistance, closeCenter.y - WindowButtons.s_closeLineDistance);
            ctx.lineTo(closeCenter.x + WindowButtons.s_closeLineDistance, closeCenter.y + WindowButtons.s_closeLineDistance);
            ctx.stroke();

            super.draw(ctx);
        }

        /**
         * Checks if a click happened on one of the buttons.
         * @param e Event to process
         * @param data Additionnal data
         */
        processEvent(e: Event, data: any): void
        {
            super.processEvent(e, data);

            switch (e.type)
            {
                case 'mousedown':
                case 'mouseup':
                    // Check if the click happened inside the close button boundaries
                    const clickPosition: Vec2D = data;
                    const position: Vec2D = this.getAbsolutePosition();
                    const closeCenter: Vec2D = {x: position.x + WindowButtons.s_closeRadius, y: position.y + WindowButtons.s_closeRadius};

                    if (Math.sqrt(Math.pow(closeCenter.x - clickPosition.x, 2) + Math.pow(closeCenter.y - clickPosition.y, 2)) <=
                        WindowButtons.s_closeRadius)
                    {
                        if (e.type === 'mousedown')
                        {
                            this.m_focused = true;
                        }
                        else if (this.m_focused)
                        {
                            this.m_buttonEventMgr.callHandlers(this, WindowButtonEvent.BUTTON_CLOSE);
                            this.m_focused = false;
                        }
                    }

                    break;
            }
        }

        /**
         * Adds an handler to the button event manager.
         * 
         * Example:
         * ```TypeScript
         * let windowButtons: WindowButtons = new WindowButtons('windowButtons', {x: 8, y: 8});
         *
         * super.addChild(windowButtons);
         * windowButtons.addButtonHandler((target: WindowButtons, type: WindowButtonEvent) => {
         *     console.log('Window button event', type, 'from', target, 'fired!');
         * });
         * ```
         * @param f Handler to add
         */
        addButtonHandler(f: Function): void { this.m_buttonEventMgr.addHandler(f); }

        /**
         * Removes an handler from the button event manager.
         * @param f Handler to remove
         */
        removeButtonHandler(f: Function): void { this.m_buttonEventMgr.removeHandler(f); }

        /**
         * Gets the size of this widget.
         * @returns The widget size
         */
        static getSize(): Vec2D { return {x: WindowButtons.s_closeRadius * 2, y: WindowButtons.s_closeRadius * 2}; }
    }

    /** Enumerates the different types of {@link Window}. */
    export enum WindowType
    {
        /** A normal window (default)*/
        TYPE_NORMAL,

        /** A window without controls */
        TYPE_DIALOG,

        /** A window without decoration */
        TYPE_BORDERLESS
    }

    /**
     * A window containing widgets that can be dragged around.
     */
    export class Window extends Widget
    {
        /** Size of the window container */
        private m_size: Vec2D;

        /** Type of window */
        private m_windowType: WindowType;

        /** Flag set when the window is currently focused */
        private m_focused: boolean;

        /** Event manager for when the window closes */
        private m_closeEventMgr: EventHandlerManager;

        /** Container of the window */
        private m_windowContent: Container;

        /** Titlebar text of the window */
        private m_titleText?: Text;

        // Some design-related constants

        /** Height of the titlebar */
        private static readonly s_titlebarHeight: number = 32;

        /** Radius of the top-corner of the window */
        private static readonly s_cornerRadius: number = 12;

        /** Left-margin for the {@link WindowButtons} */
        private static readonly s_windowButtonsLeftMargin: number = 8;

        /** Color of the titlebar text when not focused */
        private static readonly s_inactiveTextColor: string = 'rgba(255, 255, 255, 0.6)';

        /** Color of the titlebar text when focused */
        private static readonly s_activeTextColor: string = 'rgba(255, 255, 255, 0.85)';

        /**
         * Constructs a window.
         * @param id ID of the window
         * @param title Title of the window
         * @param position Position of the window
         * @param size Size of the window container
         * @param windowType Type of the window
         */
        constructor(id: string, title: string, position: Vec2D, size: Vec2D, windowType: WindowType = WindowType.TYPE_NORMAL)
        {
            super(id, position);

            this.m_size = size;
            this.m_windowType = windowType;

            this.m_closeEventMgr = new EventHandlerManager();

            const titlebarHeight: number = this.getTitlebarHeight();

            // Setup the window container
            this.m_windowContent = new Container('windowCtr', {x: 0, y: titlebarHeight}, size);
            super.addChild(this.m_windowContent);

            // Setup borders if the window is not borderless
            if (this.m_windowType !== WindowType.TYPE_BORDERLESS)
            {
                // Setup the title text
                let titleText: Text = new Text('windowTitle', title, {x: this.m_size.x / 2, y: titlebarHeight / 2});
                super.addChild(titleText);
                this.m_titleText = titleText;

                titleText.setHorizontalAlignment('center');
                titleText.setVerticalAlignment('middle');
                titleText.setTextWeight(TextWeight.WEIGHT_600);
                titleText.setTextColor(Window.s_inactiveTextColor);

                // Setup window buttons if it's not a dialog
                if (this.m_windowType !== WindowType.TYPE_DIALOG)
                {
                    // Setup the window buttons
                    const windowButtonsSize: Vec2D = WindowButtons.getSize();
                    let windowButtons: WindowButtons = new WindowButtons('windowButtons',
                        {x: this.m_size.x - windowButtonsSize.x - Window.s_windowButtonsLeftMargin, y: titlebarHeight / 2 - windowButtonsSize.y / 2});
                    
                    super.addChild(windowButtons);
                    windowButtons.addButtonHandler((target: WindowButtons, type: WindowButtonEvent) => {
                        closeWindow(this.m_id);
                    });
                        
                }
            }

            this.m_focused = false;
            this.setFocused(false);
        }

        /**
         * Finds a child in the window container.
         * @param id ID of the child
         * @returns The child, if found
         */
        findChildById(id: string): Widget | undefined
        {
            // We prefer to search for the widget in the window contents
            return this.m_windowContent.findChildById(id);
        }

        /**
         * Adds a child to the window container.
         * @param widget Widget to add
         */
        addChild(widget: Widget): void
        {
            // We prefer to add a child to the window contents, instead of the raw window
            this.m_windowContent.addChild(widget);
            this.refresh();
        }

        /**
         * Renders the window decoration (if applicable),
         * and container.
         * @param ctx Rendering context
         */
        draw(ctx: CanvasRenderingContext2D): void
        {
            const drawPosition: Vec2D = this.getAbsolutePosition();
            const titlebarHeight: number = this.getTitlebarHeight();

            // Draw the window shadow
            ctx.shadowColor = 'rgba(0, 0, 0, 0.5)';
            ctx.shadowBlur = 6;
            ctx.shadowOffsetX = 6;
            ctx.shadowOffsetY = 6;

            ctx.fillRect(drawPosition.x, drawPosition.y + Window.s_cornerRadius, this.m_size.x, this.m_size.y + titlebarHeight - Window.s_cornerRadius);
            ctx.shadowColor = 'transparent';

            // Draw the titlebar if the window is not borderless
            if (this.m_windowType !== WindowType.TYPE_BORDERLESS)
            {
                // Prepare the background color for the titlebar
                if (this.m_focused)
                {
                    let titleColor: CanvasGradient = ctx.createLinearGradient(drawPosition.x, drawPosition.y, drawPosition.x, drawPosition.y + titlebarHeight);
                    titleColor.addColorStop(0, '#303030');
                    titleColor.addColorStop(1, '#2B2B2B');
                    ctx.fillStyle = titleColor;
                }
                else
                {
                    ctx.fillStyle = '#3D3D3D';
                }

                // Draw the titlebar with the topmost corners rounded
                ctx.beginPath();
                ctx.moveTo(drawPosition.x, drawPosition.y + titlebarHeight);
                ctx.lineTo(drawPosition.x + this.m_size.x, drawPosition.y + titlebarHeight);
                ctx.lineTo(drawPosition.x + this.m_size.x, drawPosition.y + Window.s_cornerRadius);
                ctx.quadraticCurveTo(drawPosition.x + this.m_size.x, drawPosition.y, drawPosition.x + this.m_size.x - Window.s_cornerRadius, drawPosition.y);
                ctx.lineTo(drawPosition.x + Window.s_cornerRadius, drawPosition.y);
                ctx.quadraticCurveTo(drawPosition.x, drawPosition.y, drawPosition.x, drawPosition.y + Window.s_cornerRadius);
                ctx.lineTo(drawPosition.x, drawPosition.y + titlebarHeight);
                ctx.closePath();
                ctx.fill();
            }

            super.draw(ctx);
        }

        /**
         * Checks if a position is within the absolute bounds
         * of the window titlebar.
         * @param position Position to check against
         */
        isPositionOnTitlebar(position: Vec2D): boolean
        {
            return position.x >= this.m_position.x &&
                        position.x <= this.m_position.x + this.m_size.x &&
                        position.y >= this.m_position.y &&
                        position.y <= this.m_position.y + this.getTitlebarHeight();
        }

        /**
         * Prepares the window to be closed.
         * @returns ``true`` if the window is ready to close
         */
        prepareForClose(): boolean
        {
            // Call the close handlers
            this.m_closeEventMgr.callHandlers(this);

            return true;
        }

        /**
         * Adds an handler to the close event manager.
         * 
         * Example:
         * ```TypeScript
         * let win = new KwimMod.Window('win', 'Window', {x: 8, y: 8}, {x: 256, y: 256});
         * 
         * testwin.addCloseHandler((target: KwimMod.Window, retVal?: any) => {
         *     console.log('Window', target, 'closed with value', retVal);
         * });
         * ```
         * @param f Handler to add
         */
        addCloseHandler(f: Function): void { this.m_closeEventMgr.addHandler(f); }

        /**
         * Removes an handler from the close event manager.
         * @param f Handler to remove
         */
        removeCloseHandler(f: Function): void { this.m_closeEventMgr.removeHandler(f); }

        /**
         * Sets the window focus flag.
         * @param focused The new focus flag
         */
        setFocused(focused: boolean): void
        {
            this.m_focused = focused;

            this.m_titleText?.setTextColor(this.m_focused ? Window.s_activeTextColor : Window.s_inactiveTextColor);
            this.m_titleText?.setTextWeight(this.m_focused ? TextWeight.WEIGHT_600 : TextWeight.WEIGHT_NORMAL);

            this.refresh();
        }

        /**
         * Gets the window focus flag.
         * @returns The window focus flag
         */
        isFocused(): boolean { return this.m_focused; }

        /**
         * Gets the window size.
         * @returns The window size
         */
        getSize(): Vec2D { return {x: this.m_size.x, y: this.m_size.y + this.getTitlebarHeight()}; }

        /**
         * Gets the window titlebar height.
         * @returns The window titlebar height
         */
        getTitlebarHeight(): number
        {
            return (this.m_windowType !== WindowType.TYPE_BORDERLESS) ? Window.s_titlebarHeight : 0;
        }

        /**
         * Sets the window ready flag to ``true``.
         */
        markReady(): void { this.m_ready = true; this.refresh(); }
    }

    /** Enumerates the different text weight. */
    export enum TextWeight
    {
        /** Normal font weight (= {@link WEIGHT_400}) */
        WEIGHT_NORMAL,

        /** Bold font weight (= {@link WEIGHT_700}) */
        WEIGHT_BOLD,

        /** One font weight heavier than the parent element */
        WEIGHT_BOLDER,

        /** One font weight lighter than the parent element */
        WEIGHT_LIGHTER,

        /** Font weight of 100 */
        WEIGHT_100,

        /** Font weight of 200 */
        WEIGHT_200,

        /** Font weight of 300 */
        WEIGHT_300,

        /** Font weight of 400 (= {@link WEIGHT_NORMAL}) */
        WEIGHT_400,

        /** Font weight of 500 */
        WEIGHT_500,

        /** Font weight of 600 */
        WEIGHT_600,

        /** Font weight of 700 (= {@link WEIGHT_BOLD}) */
        WEIGHT_700,

        /** Font weight of 800 */
        WEIGHT_800,

        /** Font weight of 900 */
        WEIGHT_900
    }

    /**
     * Widget used to render text.
     */
    export class Text extends Widget
    {
        /** Text string of this text */
        private m_text: string;

        /** Comma-separated list of fonts to use */
        private m_font: string;

        /** Font size of the text */
        private m_fontSize: string;

        /** Text color */
        private m_color: string;

        /** Font style (italic or not) of the text */
        private m_fontStyle: string;

        /** Font variant (small-caps or not) of the text */
        private m_fontVariant: string;

        /** Font weight of the text */
        private m_fontWeight: TextWeight;

        /** Flag set if the text should be drawn outlined instead of filled */
        private m_outline: boolean;

        /** Horizontal alignment of the text */
        private m_horizontalAlignment: CanvasTextAlign;

        /** Vertical alignment of the text */
        private m_verticalAlignment: CanvasTextBaseline;

        /** Metrics of the text (available only after the first draw) */
        private m_textMetrics?: TextMetrics;

        /**
         * Constructs a text widget.
         * @param id ID of the text
         * @param text Text string of the text
         * @param position Position of the text
         */
        constructor(id: string, text: string, position: Vec2D)
        {
            super(id, position);

            this.m_text = text;

            this.m_font = 'Ubuntu';
            this.m_fontSize = '16px';
            this.m_color = 'rgba(255, 255, 255, 0.85)';

            this.m_fontStyle = 'normal';
            this.m_fontVariant = 'normal';
            this.m_fontWeight = TextWeight.WEIGHT_NORMAL;
            this.m_outline = false;

            this.m_horizontalAlignment = 'start';
            this.m_verticalAlignment = 'top';

            this.m_ready = true;
        }

        /**
         * Renders the text using the provided context.
         * @param ctx Rendering context
         */
        draw(ctx: CanvasRenderingContext2D)
        {
            let drawPosition: Vec2D = this.getAbsolutePosition();

            // Transform the position based on the alignments
            ctx.textAlign = this.m_horizontalAlignment;
            ctx.textBaseline = this.m_verticalAlignment;
            ++drawPosition.y;

            // Prepare the style
            ctx.font = this.getFontString();
            this.m_textMetrics = ctx.measureText(this.m_text);

            if (this.m_outline)
            {
                ctx.strokeStyle = this.m_color;
                ctx.lineWidth = 1;
                ctx.strokeText(this.m_text, drawPosition.x, drawPosition.y);
            }
            else
            {
                ctx.fillStyle = this.m_color;
                ctx.fillText(this.m_text, drawPosition.x, drawPosition.y);
            }

            super.draw(ctx);
        }

        /**
         * Sets the text string.
         * @param text The new text string
         */
        setText(text: string): void { this.m_text = text; this.refresh(); }

        /**
         * Gets the text string.
         * @returns The text string
         */
        getText(): string { return this.m_text; }

        /**
         * Sets the font style (italic or not).
         * @param italic The new italic text flag
         */
        setItalic(italic: boolean): void { this.m_fontStyle = (italic) ? 'italic' : 'normal'; this.refresh(); }

        /**
         * Gets the font style and checks if it's set to ``italic``.
         * @returns The italic text flag
         */
        isItalic(): boolean { return this.m_fontStyle == 'italic'; }

        /**
         * Sets the font variant (small-caps or not).
         * @param smallCaps The new small-caps text flag
         */
        setSmallCaps(smallCaps: boolean): void { this.m_fontVariant = (smallCaps) ? 'small-caps' : 'normal'; this.refresh(); }

        /**
         * Gets the font variant and checks if it's set to ``small-caps``.
         * @returns The small-caps text flag
         */
        isSmallCaps(): boolean { return this.m_fontVariant == 'small-caps'; }

        /**
         * Sets the text weight.
         * @param weight The new text weight
         */
        setTextWeight(weight: TextWeight): void { this.m_fontWeight = weight; this.refresh(); }

        /**
         * Gets the text weight.
         * @returns The text weight
         */
        getTextWeight(): TextWeight { return this.m_fontWeight }

        /**
         * Converts a {@link TextWeight} to its string equivalent.
         * @param weight The {@link TextWeight} to convert
         */
        private static fontWeightToString(weight: TextWeight): string
        {
            switch (weight)
            {
                case TextWeight.WEIGHT_BOLD: return 'bold';
                case TextWeight.WEIGHT_BOLDER: return 'bolder';
                case TextWeight.WEIGHT_LIGHTER: return 'lighter';
                case TextWeight.WEIGHT_100: return '100';
                case TextWeight.WEIGHT_200: return '200';
                case TextWeight.WEIGHT_300: return '300';
                case TextWeight.WEIGHT_400: return '400';
                case TextWeight.WEIGHT_500: return '500';
                case TextWeight.WEIGHT_600: return '600';
                case TextWeight.WEIGHT_700: return '700';
                case TextWeight.WEIGHT_800: return '800';
                case TextWeight.WEIGHT_900: return '900';
                default: return 'normal';
            }
        }

        /**
         * Sets the text horizontal alignment.
         * @param horizontalAlignment The new text horizontal alignment
         */
        setHorizontalAlignment(horizontalAlignment: CanvasTextAlign): void { this.m_horizontalAlignment = horizontalAlignment; this.refresh(); }

        /**
         * Gets the text horizontal alignment.
         * @returns The text horizontal alignment
         */
        getHorizontalAlignment(): CanvasTextAlign { return this.m_horizontalAlignment; }

        /**
         * Sets the text vertical alignment.
         * @param verticalAlignment The text vertical alignment
         */
        setVerticalAlignment(verticalAlignment: CanvasTextBaseline): void { this.m_verticalAlignment = verticalAlignment; this.refresh(); }

        /**
         * Gets the text vertical alignment.
         * @returns The text vertical alignment
         */
        getVerticalAlignment(): CanvasTextBaseline { return this.m_verticalAlignment; }

        /**
         * Sets the text outline flag.
         * @param outline The new outline flag
         */
        setOutlineMode(outline: boolean): void { this.m_outline = outline; this.refresh(); }

        /**
         * Gets the text outline flag.
         * @returns The text outline flag
         */
        isOutlineMode(): boolean { return this.m_outline; }

        /**
         * Sets the font family.
         * @param font The new font family
         */
        setFontFamily(font: string): void { this.m_font = font; this.refresh(); }

        /**
         * Gets the font family.
         * @returns The font family
         */
        getFontFamily(): string { return this.m_font; }

        /**
         * Sets the text color.
         * @param textColor The new text color
         */
        setTextColor(textColor: string): void { this.m_color = textColor; this.refresh(); }

        /**
         * Gets the text color.
         * @returns The text color
         */
        getTextColor(): string { return this.m_color; }

        /**
         * Sets the font size.
         * @param fontSize The new font size
         */
        setFontSize(fontSize: string): void { this.m_fontSize = fontSize; this.refresh(); }

        /**
         * Gets the font size.
         * @returns The font size
         */
        getFontSize(): string { return this.m_fontSize; }

        /**
         * Gets the font string from all the text style parameters,
         * to be used in ``CanvasRenderingContext2D``'s ``font`` property.
         */
        private getFontString(): string
        {
            return this.m_fontStyle + ' ' + this.m_fontVariant + ' ' +
                        Text.fontWeightToString(this.m_fontWeight) + ' ' + this.m_fontSize + ' ' + this.m_font +
                        ', -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", \
                        "Helvetica Neue", Arial, sans-serif, \
                        "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"';
        }

        /**
         * Gets the text metrics.
         * 
         * Note that those are only available after the first draw.
         * @returns The text metrics, if available.
         */
        getTextMetrics(): TextMetrics | undefined { return this.m_textMetrics; }
    }

    /**
     * A widget representing a value between a minimum and a maximum,
     * often used to display the progress of an operation.
     */
    export class ProgressBar extends Widget
    {
        /** Size of the progress bar */
        protected m_size: Vec2D;

        /** Actual progress */
        protected m_progress: number;

        /** Minimum progress */
        protected m_minProgress: number;

        /** Maximum progress */
        protected m_maxProgress: number;

        /** Corner radius of the progress bar */
        private static readonly s_cornerRadius: number = 4;

        /**
         * Constructs a new progress bar.
         * @param id ID of the progress bar
         * @param position Position of the progress bar
         * @param size Size of the progress bar
         * @param progress Actual progress of the bar
         */
        constructor(id: string, position: Vec2D, size: Vec2D, progress: number = 0)
        {
            super(id, position);

            this.m_size = size;

            this.m_progress = progress;
            this.m_minProgress = 0;
            this.m_maxProgress = 1;

            this.m_ready = true;
        }

        /**
         * Renders the progress bar using the provided context.
         * @param ctx Rendering context
         */
        draw (ctx: CanvasRenderingContext2D)
        {
            let drawPosition: Vec2D = this.getAbsolutePosition();

            // Draw the progress bar background
            ctx.fillStyle = '#676767';
            Widget.drawRoundedRectangle(ctx, drawPosition, this.m_size, ProgressBar.s_cornerRadius);

            // Draw the progress bar foreground
            let progressColor = ctx.createLinearGradient(drawPosition.x, drawPosition.y, drawPosition.x, drawPosition.y + this.m_size.y);
            progressColor.addColorStop(0, '#00ACFF');
            progressColor.addColorStop(1, '#008CFF');
            ctx.fillStyle = progressColor;
            
            const progressSize: Vec2D = {
                x: this.getNormalizedProgress() * this.m_size.x,
                y: this.m_size.y
            };
            
            if (progressSize.x > 0)
                Widget.drawRoundedRectangle(ctx, drawPosition, progressSize, ProgressBar.s_cornerRadius);

            super.draw(ctx);
        }

        /**
         * Sets the size of the progress bar.
         * @param size The new size of the progress bar
         */
        setSize(size: Vec2D): void { this.m_size = size; this.refresh(); }

        /**
         * Gets the size of the progress bar.
         * @returns The size of the progress bar
         */
        getSize(): Vec2D { return this.m_size; }

        /**
         * Sets the actual progress.
         * 
         * The value will get clamped if outside of valid range.
         * @param progress The new progress
         */
        setProgress(progress: number): void
        {
            this.m_progress = Math.max(this.m_minProgress, Math.min(progress, this.m_maxProgress));

            this.refresh();
        }

        /**
         * Gets the actual progress.
         * @returns The actual progress
         */
        getProgress(): number { return this.m_progress; }

        /**
         * Sets a normalized progress, where ``0 <= normalizedProgress <= 1``, with
         * ``0`` representing the minimum progress and
         * ``1`` representing the maximum progress.
         * 
         * The value will get clamped if outside of valid range.
         * @param normalizedProgress The new normalized progress
         */
        setNormalizedProgress(normalizedProgress: number): void
        {
            // Clamp the normalized progress
            normalizedProgress = Math.max(0, Math.min(normalizedProgress, 1));

            this.m_progress = normalizedProgress * (this.m_maxProgress - this.m_minProgress) + this.m_minProgress;
            this.refresh();
        }
        
        /**
         * Computes the normalized progress, between ``0`` and ``1``.
         * @returns The normalized progress
         */
        getNormalizedProgress(): number { return Math.min(Math.max(0, (this.m_progress - this.m_minProgress) / (this.m_maxProgress - this.m_minProgress)), 1); }

        /**
         * Sets the minimum progress.
         * 
         * If the new minimum progress is greater than the maximum progress,
         * both values will get swapped.
         * @param minProgress The new minimum progress
         */
        setMinProgress(minProgress: number): void
        {
            this.m_minProgress = minProgress;
            if (this.m_minProgress > this.m_maxProgress)
                this.m_minProgress = [this.m_maxProgress, this.m_maxProgress = this.m_minProgress][0];

            this.refresh();
        }

        /**
         * Gets the minimum progress.
         * @returns The minimum progress
         */
        getMinProgress(): number { return this.m_minProgress; }

        /**
         * Sets the maximum progress.
         * 
         * If the new maximum progress is less than the minimum progress,
         * both values will get swapped.
         * @param maxProgress The new maximum progress
         */
        setMaxProgress(maxProgress: number): void
        {
            this.m_maxProgress = maxProgress;
            if (this.m_maxProgress < this.m_minProgress)
                this.m_maxProgress = [this.m_minProgress, this.m_minProgress = this.m_maxProgress][0];

            this.refresh();
        }

        /**
         * Gets the maximum progress.
         * @returns The maximum progress
         */
        getMaxProgress(): number { return this.m_maxProgress; }
    }

    /**
     * Represents a static button the user can click on to perform an action.
     */
    export class Button extends Widget
    {
        /** Size of the button */
        private m_size: Vec2D;

        /** Flag set when the button can be clicked */
        private m_enabled: boolean;

        /** Flag indicating if the button has focus */
        private m_focused: boolean;

        /** Text inside the button */
        private m_buttonText: Text;

        /** Event manager when the user clicks on the button */
        private m_clickEventMgr: EventHandlerManager;

        /** The corner radius of the button */
        private static readonly s_cornerRadius: number = 4;

        /**
         * Constructs a new button.
         * @param id ID of the button
         * @param text Text of the button
         * @param position Position of the button
         * @param size Size of the button
         */
        constructor(id: string, text: string, position: Vec2D, size: Vec2D)
        {
            super(id, position);

            this.m_clickEventMgr = new EventHandlerManager();

            this.m_size = size;
            this.m_enabled = true;
            this.m_focused = false;

            // Setup the button text
            let buttonText: Text = new Text('buttonText', text, {x: size.x / 2, y: size.y / 2});
            this.addChild(buttonText);
            this.m_buttonText = buttonText;

            buttonText.setHorizontalAlignment('center');
            buttonText.setVerticalAlignment('middle');

            this.m_ready = true;
        }

        /**
         * Renders the button using the provided context.
         * @param ctx Rendering context
         */
        draw(ctx: CanvasRenderingContext2D): void
        {
            let drawPosition: Vec2D = this.getAbsolutePosition();

            // Draw the button background
            ctx.fillStyle = (this.m_enabled) ? '#5D5D5D' : '#5D5D5D88';
            Widget.drawRoundedRectangle(ctx, drawPosition, this.m_size, Button.s_cornerRadius);

            super.draw(ctx);
        }

        /**
         * Checks if the button has been clicked.
         * @param e Event to process
         * @param data Additionnal data
         */
        processEvent(e: Event, data: any): void
        {
            super.processEvent(e, data);

            if (!this.m_enabled) return;

            switch (e.type)
            {
                case 'mousedown':
                case 'mouseup':
                    {
                        // Check if the click happened inside the button boundaries
                        const clickPosition: Vec2D = data;
                        const position: Vec2D = this.getAbsolutePosition();

                        if (clickPosition.x >= position.x && clickPosition.x <= position.x + this.m_size.x &&
                            clickPosition.y >= position.y && clickPosition.y <= position.y + this.m_size.y)
                        {
                            if (e.type === 'mousedown')
                            {
                                this.m_focused = true;
                            }
                            else if (this.m_focused)
                            {
                                this.m_clickEventMgr.callHandlers(this);
                                this.m_focused = false;
                            }
                        }
                    }

                    break;
            }
        }

        /**
         * Adds an handler to the click event manager.
         * 
         * Example:
         * ```TypeScript
         * let button: KwimMod.Button = new KwimMod.Button('button', 'Button', {x: 8, y: 8}, {x: 64, y: 24});
         * 
         * button.addClickHandler((target: KwimMod.Button) => {
         *     console.log('Button', target, 'clicked!');
         * });
         * ```
         * @param f Handler to add
         */
        addClickHandler(f: Function): void { this.m_clickEventMgr.addHandler(f); }

        /**
         * Removes an handler from the click event manager.
         * @param f Handler to remove
         */
        removeClickHandler(f: Function): void { this.m_clickEventMgr.removeHandler(f); }

        /**
         * Sets the enabled flag.
         * @param enabled The new enabled flag
         */
        setEnabled(enabled: boolean): void
        {
            this.m_enabled = enabled;
            this.m_buttonText.setTextColor((this.m_enabled) ? 'rgba(255, 255, 255, 0.85)' : 'rgba(255, 255, 255, 0.5)');

            this.refresh();
        }

        /**
         * Gets the enabled flag.
         * @returns The enabled flag
         */
        isEnabled(): boolean { return this.m_enabled; }
    }

    /**
     * Represents a progress bar with a thumb that the user can drag
     * to set a numerical value between a minimum and a maximum.
     */
    export class SeekBar extends ProgressBar
    {
        /** Flag set when the widget is focused */
        private m_focused: boolean;

        /** Event manager when the user changes the value */
        private m_progressEventMgr: EventHandlerManager;

        /** Multiplier for the thumb radius */
        private static readonly s_radiusMultiplier = 1.2;

        /**
         * Constructs a new seek bar.
         * @param id ID of the seek bar
         * @param position Position of the seek bar
         * @param size Size of the seek bar
         */
        constructor(id: string, position: Vec2D, size: Vec2D)
        {
            super(id, position, size, 0);

            this.m_focused = false;
            this.m_progressEventMgr = new EventHandlerManager();

            this.m_ready = true;
        }

        /**
         * Renders the seek bar using the provided context
         * @param ctx Rendering context
         */
        draw(ctx: CanvasRenderingContext2D)
        {
            super.draw(ctx);

            // Prepare coordinates
            const drawPosition: Vec2D = this.getAbsolutePosition();
            const thumbPosition: Vec2D = this.getThumbCenterPosition();

            // Draw the seekbar thumb
            ctx.fillStyle = '#8D8D8D';
            ctx.strokeStyle = '#6D6D6D';
            ctx.lineWidth = 2;
            ctx.beginPath();
            ctx.arc(thumbPosition.x, thumbPosition.y, this.m_size.y * SeekBar.s_radiusMultiplier, 0, 2 * Math.PI);
            ctx.closePath();
            ctx.fill();
            ctx.stroke();
        }

        /**
         * Checks if the seek bar or its thumb has been clicked.
         * 
         * If the seek bar is focused, computes a new progress when the mouse moves,
         * based on its X position.
         * @param e Event to process
         * @param data Additionnal data
         */
        processEvent(e: Event, data: any): void
        {
            super.processEvent(e, data);

            switch (e.type)
            {
                case 'mousedown':
                    // Check if the click happened inside the seekbar/seekbar thumb boundaries
                    const clickPosition: Vec2D = data;
                    const absolutePosition: Vec2D = this.getAbsolutePosition();
                    const thumbPosition: Vec2D = this.getThumbCenterPosition();


                    if ((clickPosition.x >= absolutePosition.x && clickPosition.x <= absolutePosition.x + this.m_size.x &&
                        clickPosition.y >= absolutePosition.y && clickPosition.y <= absolutePosition.y + this.m_size.y) ||
                        (Math.sqrt(Math.pow(clickPosition.x - thumbPosition.x, 2) + Math.pow(clickPosition.y - thumbPosition.y, 2)) <=
                        this.m_size.y * SeekBar.s_radiusMultiplier))
                    {
                        this.m_focused = true;

                        const saneMouseX: number = Math.min(Math.max(0, clickPosition.x - absolutePosition.x), this.m_size.x);
                        const normalizedProgress = saneMouseX / this.m_size.x;
                        
                        // Update the thumb and progress
                        this.setNormalizedProgress(normalizedProgress);
                    }

                    break;

                case 'mousemove':
                    if (this.m_focused)
                    {
                        // Calculate where the thumb should be
                        const mousePosition: Vec2D = data;
                        const absolutePosition: Vec2D = this.getAbsolutePosition();
                        const saneMouseX: number = Math.min(Math.max(0, mousePosition.x - absolutePosition.x), this.m_size.x);
                        const normalizedProgress = saneMouseX / this.m_size.x;
                        
                        // Update the thumb and progress
                        this.setNormalizedProgress(normalizedProgress);
                    }

                    break;

                case 'mouseup':
                    this.m_focused = false;
                    break;
            }
        }

        /**
         * Computes the position of the center of the thumb.
         * @returns The position of the center of the thumb
         */
        private getThumbCenterPosition(): Vec2D
        {
            const absolutePosition: Vec2D = this.getAbsolutePosition();
            return {x: this.getNormalizedProgress() * this.m_size.x + absolutePosition.x, y: absolutePosition.y + (this.m_size.y / 2)};
        }

        /**
         * Adds an handler to the progress event manager.
         * 
         * Example:
         * ```TypeScript
         * let seekbar: KwimMod.SeekBar = new KwimMod.SeekBar('seekbar', {x: 8, y: 8}, {x: 128, y: 8});
         * 
         * seekbar.addProgressHandler((target: KwimMod.SeekBar, normProgress: number) => {
         *     console.log('Seekbar', target, 'updated its progress', normProgress);
         * });
         * ```
         * @param f Handler to add
         */
        addProgressHandler(f: Function): void { this.m_progressEventMgr.addHandler(f); }

        /**
         * Removes an handler to the progress event manager.
         * @param f Handler to remove
         */
        removeProgressHandler(f: Function): void { this.m_progressEventMgr.removeHandler(f); }

        /**
         * Sets a normalized progress, where ``0 <= normalizedProgress <= 1``, with
         * ``0`` representing the minimum progress and
         * ``1`` representing the maximum progress.
         * @param normalizedProgress The new normalized progress
         * @param sendEvent Whether to send an event because the progress changed (defaults to ``true``)
         */
        setNormalizedProgress(normalizedProgress: number, sendEvent: boolean = true)
        {
            const previousProgress: number = this.m_progress;
            super.setNormalizedProgress(normalizedProgress);

            if (previousProgress != this.m_progress && sendEvent)
                this.m_progressEventMgr.callHandlers(this, this.m_progress);
        }

        /**
         * Sets the progress of the seek bar.
         * @param progress The new progress
         * @param sendEvent Whether to send an event because the progress changed (defaults to ``true``)
         */
        setProgress(progress: number, sendEvent: boolean = true)
        {
            if (progress != this.m_progress)
            {
                super.setProgress(progress);
                if (sendEvent)
                    this.m_progressEventMgr.callHandlers(this, this.m_progress);
            }
        }
    }

    /**
     * Base class representing any widget holding a binary state.
     */
    abstract class BaseCheckBox extends Widget
    {
        /** State of the checkbox */
        protected m_checked: boolean;

        /** Event manager for when the state changes */
        private m_checkedEventMgr: EventHandlerManager;

        /**
         * Constructs a new base checkbox.
         * @param id ID of the base checkbox
         * @param position Position of the base checkbox
         * @param checked Initial state of the base checkbox
         */
        constructor(id: string, position: Vec2D, checked: boolean = false)
        {
            super(id, position);

            this.m_checked = checked;

            this.m_checkedEventMgr = new EventHandlerManager();
        }

        /**
         * Sets the base checkbox state.
         * @param checked The new state
         */
        setChecked(checked: boolean): void
        {
            if (this.m_checked !== checked)
            {
                this.m_checked = checked;

                this.m_checkedEventMgr.callHandlers(this, checked);
                this.refresh();
            }
        }

        /**
         * Gets the base checkbox state.
         * @returns The state
         */
        isChecked(): boolean { return this.m_checked; }

        /**
         * Adds an handler to the checked event manager.
         * 
         * Example:
         * ```TypeScript
         * let checkbox: KwimMod.CheckBox = new KwimMod.CheckBox('checkbox', 'Checkbox', {x: 8, y: 8});
         * 
         * checkbox.addCheckedHandler((target: KwimMod.CheckBox, checked: boolean) => {
         *     console.log('Checkbox', target, 'updated its state', checked);
         * });
         * ```
         * @param f Handler to add
         */
        addCheckedHandler(f: Function): void { this.m_checkedEventMgr.addHandler(f); }

        /**
         * Removes an handler to the checked event manager.
         * @param f Handler to remove
         */
        removeCheckedHandler(f: Function): void { this.m_checkedEventMgr.removeHandler(f); }
    }

    /**
     * Represents a box that the user can tick and untick.
     */
    export class CheckBox extends BaseCheckBox
    {
        /** Flag indicating if the checkbox has focus */
        private m_focused: boolean;

        /** Text of the checkbox */
        private m_checkboxText: Text;

        /** Size of the checkbox */
        private static readonly s_checkboxSize: number = 16;

        /** Margin of the checkmark inside the checkbox */
        private static readonly s_checkmarkMargin: number = 2;

        /**
         * Constructs a new checkbox
         * @param id ID of the checkbox
         * @param text Text of the checkbox
         * @param position Position of the checkbox
         * @param checked Initial state of the checkbox
         */
        constructor(id: string, text: string, position: Vec2D, checked: boolean = false)
        {
            super(id, position, checked);

            this.m_focused = false;

            // Setup the checkbox text
            let checkboxText: Text = new Text('checkboxText', text, {x: CheckBox.s_checkboxSize + 4, y: CheckBox.s_checkboxSize / 2});
            this.addChild(checkboxText);
            this.m_checkboxText = checkboxText;

            checkboxText.setVerticalAlignment('middle');

            this.m_ready = true;
        }

        /**
         * Renders the checkbox, and its mark if checked, using the provided context.
         * @param ctx Rendering context
         */
        draw(ctx: CanvasRenderingContext2D)
        {
            let drawPosition: Vec2D = this.getAbsolutePosition();

            // Draw the checkbox background
            ctx.fillStyle = '#5D5D5D';
            ctx.fillRect(drawPosition.x, drawPosition.y, CheckBox.s_checkboxSize, CheckBox.s_checkboxSize);

            // Check if the checkbox is checked
            if (this.m_checked)
            {
                // Prepare the checkmark draw boundaries
                const checkmarkPosition: Vec2D = {x: drawPosition.x + CheckBox.s_checkmarkMargin, y: drawPosition.y + CheckBox.s_checkmarkMargin};
                const checkmarkSize: Vec2D = {x: CheckBox.s_checkboxSize - CheckBox.s_checkmarkMargin * 2, y: CheckBox.s_checkboxSize - CheckBox.s_checkmarkMargin * 2}

                // Prepare the checkmark color and draw it
                let progressColor = ctx.createLinearGradient(checkmarkPosition.x, checkmarkPosition.y, checkmarkPosition.x, checkmarkPosition.y + checkmarkSize.y);
                progressColor.addColorStop(0, '#00A6FF');
                progressColor.addColorStop(1, '#0092FF');
                ctx.fillStyle = progressColor;

                ctx.fillRect(checkmarkPosition.x, checkmarkPosition.y, checkmarkSize.x, checkmarkSize.y);
            }

            super.draw(ctx);
        }

        /**
         * Checks if a click happened inside the checkbox,
         * and invert its state if so.
         * @param e Event to process
         * @param data Additionnal data
         */
        processEvent(e: Event, data: any)
        {
            super.processEvent(e, data);

            switch (e.type)
            {
                case 'mousedown':
                case 'mouseup':
                    {
                        // Check if the click happened inside the checkbox boundaries
                        const clickPosition: Vec2D = data;
                        const position: Vec2D = this.getAbsolutePosition();
                        const checkboxSize: Vec2D = this.getSize();

                        if (clickPosition.x >= position.x && clickPosition.x <= position.x + checkboxSize.x &&
                            clickPosition.y >= position.y && clickPosition.y <= position.y + checkboxSize.y)
                        {
                            if (e.type === 'mousedown')
                            {
                                this.m_focused = true;
                            }
                            else if (this.m_focused)
                            {
                                this.setChecked(!this.isChecked());
                                this.m_focused = false;
                            }
                        }
                    }

                    break;
            }
        }

        /**
         * Gets the size of the checkbox.
         * @returns The checkbox size
         */
        getSize(): Vec2D
        {
            const textMeasures: TextMetrics | undefined = this.m_checkboxText.getTextMetrics();
            return {
                x: CheckBox.s_checkboxSize + 4 + ((textMeasures !== undefined) ? textMeasures.width : 0),
                y: CheckBox.s_checkboxSize
            };
        }
    }

    /**
     * Represents a box that the user can only tick.
     */
    export class RadioButton extends BaseCheckBox
    {
        /** Flag indicating if the radio button has focus */
        private m_focused: boolean;

        /** Text of the radio button */
        private m_radiobtnText: Text;

        /** Radius of the radio button */
        private static readonly s_radiobtnRadius: number = 8;

        /** Margin of the mark inside the radio button */
        private static readonly s_radiomarkMargin: number = 2;

        /**
         * Constructs a new radio button.
         * @param id ID of the radio button
         * @param text Text of the radio button
         * @param position Position of the radio button
         * @param checked Initial state of the radio button
         */
        constructor(id: string, text: string, position: Vec2D, checked: boolean = false)
        {
            super(id, position, checked);

            this.m_focused = false;

            // Setup the checkbox text
            let checkboxText: Text = new Text('radiobuttonText', text, {x: RadioButton.s_radiobtnRadius + 12, y: RadioButton.s_radiobtnRadius});
            this.addChild(checkboxText);
            this.m_radiobtnText = checkboxText;

            checkboxText.setVerticalAlignment('middle');

            this.m_ready = true;
        }

        /**
         * Renders the radio button, and its mark if checked, using the provided context.
         * @param ctx Rendering context
         */
        draw(ctx: CanvasRenderingContext2D)
        {
            const drawPosition: Vec2D = this.getAbsolutePosition();
            const radiobtnCenter: Vec2D = {x: drawPosition.x + RadioButton.s_radiobtnRadius, y: drawPosition.y + RadioButton.s_radiobtnRadius};

            // Draw the radiobtn background
            ctx.fillStyle = '#5D5D5D';
            Widget.drawCircle(ctx, radiobtnCenter, RadioButton.s_radiobtnRadius);

            // Check if the radiobtn is checked
            if (this.m_checked)
            {
                // Prepare the radiomark color and draw it
                let progressColor = ctx.createLinearGradient(radiobtnCenter.x - RadioButton.s_radiobtnRadius, radiobtnCenter.y - RadioButton.s_radiobtnRadius,
                    radiobtnCenter.x + RadioButton.s_radiobtnRadius, radiobtnCenter.y + RadioButton.s_radiobtnRadius);
                progressColor.addColorStop(0, '#00A6FF');
                progressColor.addColorStop(1, '#0092FF');
                ctx.fillStyle = progressColor;

                Widget.drawCircle(ctx, radiobtnCenter, RadioButton.s_radiobtnRadius - RadioButton.s_radiomarkMargin);
            }

            super.draw(ctx);
        }

        /**
         * Checks if a click happened inside the radio button,
         * and ticks the button if so.
         * @param e Event to process
         * @param data Additionnal data
         */
        processEvent(e: Event, data: any)
        {
            super.processEvent(e, data);

            switch (e.type)
            {
                case 'mousedown':
                case 'mouseup':
                    // Check if the click happened inside the radiobtn boundaries
                    const clickPosition: Vec2D = data;
                    const position: Vec2D = this.getAbsolutePosition();
                    const radiobtnSize: Vec2D = this.getSize();

                    if (clickPosition.x >= position.x && clickPosition.x <= position.x + radiobtnSize.x &&
                        clickPosition.y >= position.y && clickPosition.y <= position.y + radiobtnSize.y)
                    {
                        if (e.type === 'mousedown')
                        {
                            this.m_focused = true;
                        }
                        else if (this.m_focused)
                        {
                            this.setChecked(true);
                            this.m_focused = false;
                        }
                    }

                    break;
            }
        }

        /**
         * Gets the size of the radio button.
         * @returns The radio button size
         */
        getSize(): Vec2D
        {
            const textMeasures: TextMetrics | undefined = this.m_radiobtnText.getTextMetrics();
            return {
                x: (RadioButton.s_radiobtnRadius * 2) + 8 + ((textMeasures !== undefined) ? textMeasures.width : 0),
                y: (RadioButton.s_radiobtnRadius * 2)
            };
        }
    }

    /**
     * This widget manages a group of radio buttons
     * to provide the user a list of mutually-exclusive choices.
     */
    export class RadioButtonGroup extends Widget
    {
        /** Event handler for when the choice has changed */
        private m_choiceEventMgr: EventHandlerManager;

        /** Vertical spacing of the radio buttons */
        private static readonly s_radioSpacing = 4;

        /**
         * Constructs a new radio button group
         * @param id ID of the radio button group
         * @param position Position of the radio button group
         */
        constructor(id: string, position: Vec2D)
        {
            super(id, position);

            this.m_choiceEventMgr = new EventHandlerManager();

            this.m_ready = true;
        }

        /**
         * Adds a radio button to the group.
         * @param id ID of the radio button to add
         * @param text Text of the radio button to add
         */
        addRadioButton(id: string, text: string)
        {
            const lastChild: RadioButton | undefined = this.m_children[this.m_children.length - 1] as RadioButton;
            const radioHeight = (lastChild !== undefined) ? lastChild.getSize().y : 0;

            let radio: RadioButton = new RadioButton(id, text, {x: 0, y: (radioHeight + RadioButtonGroup.s_radioSpacing) * this.m_children.length});
            this.addChild(radio);

            radio.addCheckedHandler((target: RadioButton, checked: boolean) => {
                if (checked)
                {
                    // Uncheck every other child
                    for (let child of this.m_children)
                    {
                        if (child.getId() !== target.getId() && (child as RadioButton).isChecked())
                        {
                            (child as RadioButton).setChecked(false);
                        }
                    }

                    this.m_choiceEventMgr.callHandlers(target, checked);
                }
            });

            this.refresh();
        }

        /**
         * Adds an handler to the choice event manager.
         * 
         * Example:
         * ```TypeScript
         * let radiogroup: KwimMod.RadioButtonGroup = new KwimMod.RadioButtonGroup('radiogroup', {x: 8, y: 8});
         * 
         * radiogroup.addChoiceHandler((target: KwimMod.RadioButton) => {
         *     console.log('Radio group radiobutton', target, 'now checked');
         * });
         * ```
         * @param f Handler to add
         */
        addChoiceHandler(f: Function): void { this.m_choiceEventMgr.addHandler(f); }

        /**
         * Removes an handler to the choice event manager.
         * @param f Handler to remove
         */
        removeChoiceHandler(f: Function): void { this.m_choiceEventMgr.removeHandler(f); }
    }

    /**
     * Provides the user an area to type, delete, select and copy/paste.
     */
    export class TextBox extends Widget
    {
        /** Size of the textbox */
        private m_size: Vec2D;

        /** Text of the textbox */
        private m_text: string;

        /** Input predicate of the textbox, used to validate a new input */
        private m_textInputPredicate?: Function;

        /** Position of the cursor */
        private m_cursorPos: number;

        /** Position of the selection cursor */
        private m_selectionStart: number;

        /** Offset of the text inside the textbox, on the X axis */
        private m_textOffset: number;

        /** ID of the interval for mouse-based selection */
        private m_selectionIntervalId?: number;

        /** Flag indicating if the mouse is selecting text */
        private m_mouseSelecting: boolean;

        /** Timestamp of the last mousedown event */
        private m_lastMouseDown?: number;

        /** Flag indicating if the textbox has focus */
        private m_focused: boolean;

        /** ID of the interval for text cursor blinking */
        private m_cursorIntervalId?: number;

        /** Flag indicating if the text cursor is drawn */
        private m_cursorDrawn: boolean;

        /** Canvas containing the textbox */
        private m_canvas: HTMLCanvasElement;

        /** Event manager for when the text has been modified */
        private m_textEventMgr: EventHandlerManager;

        /** Radius of the textbox corners */
        private static readonly s_textboxRadius = 4;

        /** Padding of the text inside the textbox */
        private static readonly s_textboxPadding = 4;

        /**
         * Constructs a new textbox.
         * @param id ID of the textbox
         * @param position Position of the textbox
         * @param size Size of the textbox
         */
        constructor(id: string, position: Vec2D, size: Vec2D)
        {
            super(id, position);

            this.m_size = size;
            this.m_text = '';

            this.m_cursorPos = 0;
            this.m_selectionStart = 0;
            this.m_textOffset = 0;

            this.m_mouseSelecting = false;

            this.m_focused = false;
            this.m_cursorDrawn = false;

            // Setup the local canvas
            this.m_canvas = document.createElement('canvas');
            this.m_canvas.width = this.m_size.x;
            this.m_canvas.height = this.m_size.y;

            let localCtx: CanvasRenderingContext2D | null = this.m_canvas.getContext('2d');
            if (localCtx !== null)
            {
                localCtx.font = '16px Ubuntu, -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", \
                            "Helvetica Neue", Arial, sans-serif, \
                            "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"';
            }

            this.m_textEventMgr = new EventHandlerManager();

            this.m_ready = true;
        }

        /**
         * Renders the textbox background, its text, its cursor if applicable, and the selection box.
         * @param ctx Rendering context
         */
        draw(ctx: CanvasRenderingContext2D)
        {
            let localCtx: CanvasRenderingContext2D | null = this.m_canvas.getContext('2d');
            
            if (localCtx !== null)
            {
                const drawPosition: Vec2D = this.getAbsolutePosition();

                // Draw the textbox background
                localCtx.fillStyle = '#5D5D5D';
                Widget.drawRoundedRectangle(localCtx, {x: 0, y: 0}, this.m_size, TextBox.s_textboxRadius);

                // Get some measures
                const textMeasures: TextMetrics = localCtx.measureText(this.m_text);
                let textTilCursorMeasures: TextMetrics = textMeasures;

                if (this.m_cursorPos != this.m_text.length)
                    textTilCursorMeasures = localCtx.measureText(this.m_text.slice(0, this.m_cursorPos));

                // Draw the selection box, text and cursor
                if (this.m_selectionStart != this.m_cursorPos)
                {
                    localCtx.fillStyle = '#00A6FF';

                    const startSlice: number = Math.min(this.m_cursorPos, this.m_selectionStart);
                    const endSlice: number = Math.max(this.m_cursorPos, this.m_selectionStart);

                    let widthToSelect: number = localCtx.measureText(this.m_text.slice(startSlice, endSlice)).width;
                    if (this.m_selectionStart > this.m_cursorPos)
                        widthToSelect = -widthToSelect;

                    let textTilSelectStartsMeasures: TextMetrics = localCtx.measureText(this.m_text.slice(0, this.m_selectionStart));
                    localCtx.fillRect(textTilSelectStartsMeasures.width - this.m_textOffset + TextBox.s_textboxPadding, TextBox.s_textboxPadding,
                        widthToSelect, this.m_size.y - (TextBox.s_textboxPadding * 2));
                }

                localCtx.fillStyle = 'rgba(255, 255, 255, 0.85)';
                localCtx.textBaseline = 'middle';
                localCtx.fillText(this.m_text, 0 - this.m_textOffset + TextBox.s_textboxPadding, this.m_size.y / 2 + 1);
                
                if (this.m_cursorDrawn)
                    localCtx.fillRect(textTilCursorMeasures.width - this.m_textOffset + TextBox.s_textboxPadding, TextBox.s_textboxPadding, 1, this.m_size.y - (TextBox.s_textboxPadding * 2));

                // Draw the local canvas to the main one
                ctx.drawImage(this.m_canvas, drawPosition.x, drawPosition.y);
            }

            super.draw(ctx);
        }

        /**
         * Processes all the events that the textbox might be interested in:
         * 
         * - Keyboard events to insert/remove characters and perform special actions on key shortcuts
         * - Mouse events to handle focus and text selection
         * - Clipboard events to manage copy/cut/paste
         * @param e Event to process
         * @param data Additionnal data
         */
        processEvent(e: Event, data: any)
        {
            super.processEvent(e, data);

            switch (e.type)
            {
                case 'keydown':
                    if (!this.m_focused) break;

                    let ke: KeyboardEvent = e as KeyboardEvent;
                    switch (ke.key)
                    {
                        case 'Backspace':
                            if (this.m_text.length > 0 && (this.m_cursorPos > 0 || this.m_cursorPos != this.m_selectionStart))
                            {
                                // Remove the text before the cursor, or the selected text if applicable
                                if (!ke.ctrlKey)
                                    this.removeText(-1);
                                else
                                    this.removeText(this.retrieveWord(this.m_cursorPos, false) - this.m_cursorPos);
                            }

                            break;

                        case 'ArrowLeft':
                            if (this.m_cursorPos > 0)
                            {
                                let ke: KeyboardEvent = e as KeyboardEvent;

                                // Move the cursor left
                                if (!ke.ctrlKey)
                                    --this.m_cursorPos;
                                else
                                    this.m_cursorPos = this.retrieveWord(this.m_cursorPos, false);

                                if (!ke.shiftKey)
                                    this.m_selectionStart = this.m_cursorPos;

                                this.m_cursorDrawn = true;
                            }

                            break;

                        case 'ArrowRight':
                            if (this.m_cursorPos < this.m_text.length)
                            {
                                let ke: KeyboardEvent = e as KeyboardEvent;

                                // Move the cursor right
                                if (!ke.ctrlKey)
                                    ++this.m_cursorPos;
                                else
                                    this.m_cursorPos = this.retrieveWord(this.m_cursorPos, true);

                                if (!ke.shiftKey)
                                    this.m_selectionStart = this.m_cursorPos;

                                this.m_cursorDrawn = true;
                            }

                            break;

                        case 'Enter':
                            // Enter has been pressed, we can exit the textbox
                            this.setFocused(false);

                            this.m_textEventMgr.callHandlers(this, true);

                            break;

                        case 'Delete':
                            if (this.m_cursorPos < this.m_text.length)
                            {
                                // Remove the character after the cursor
                                if (!ke.ctrlKey)
                                    this.removeText(1);
                                else
                                    this.removeText(this.retrieveWord(this.m_cursorPos, true) - this.m_cursorPos);
                            }

                            break;

                        case 'Home':
                            // Set the cursor position to 0
                            this.m_cursorPos = 0;

                            if (!ke.shiftKey)
                                this.m_selectionStart = this.m_cursorPos;

                            break;

                        case 'End':
                            // Set the cursor position to the end
                            this.m_cursorPos = this.m_text.length;

                            if (!ke.shiftKey)
                                this.m_selectionStart = this.m_cursorPos;

                            break;

                        default:
                            // If the key length is > 1, it's probably a special key, so ignore it
                            if (ke.key.length > 1) break;

                            // Check if the Ctrl key is pressed before anything
                            if (!ke.ctrlKey)
                            {
                                // Add the typed characters where the cursor is, replacing any selected text
                                const typedStr: string = ke.key;
                                this.insertText(typedStr);
                            }
                            else
                            {
                                // Ctrl key is being pressed, take a special action
                                switch (ke.code)
                                {
                                    case 'KeyQ':
                                        // Select all
                                        this.m_selectionStart = 0;
                                        this.m_cursorPos = this.m_text.length;

                                        break;
                                }
                            }
                    
                            break;
                    }

                    // Refresh the widget
                    this.updateTextOffset();
                    this.refresh();

                    // Prevent browser from doing more stuff
                    if (!ke.ctrlKey || (ke.code !== 'KeyX' && ke.code !== 'KeyC' && ke.code !== 'KeyV'))
                        e.preventDefault();

                    break;

                case 'mousedown':
                    const position: Vec2D = this.getAbsolutePosition();
                    const mousePosition: Vec2D = data;
                    const me: MouseEvent = e as MouseEvent;

                    if (mousePosition.x >= position.x && mousePosition.x <= position.x + this.m_size.x &&
                            mousePosition.y >= position.y && mousePosition.y <= position.y + this.m_size.y)
                    {
                        // Click happened inside the textbox
                        this.setFocused(true);

                        // Set the cursor where the click happened
                        const cursorPos: number = this.absPositionXToCharacterNbr(mousePosition.x);

                        // Check if a fast click happened
                        if (this.m_lastMouseDown !== undefined && me.timeStamp - this.m_lastMouseDown < 300)
                        {
                            if (this.m_cursorPos === cursorPos)
                            {
                                // Double click, select the current word
                                this.m_selectionStart = this.retrieveWord(Math.min(this.m_cursorPos + 1, this.m_text.length), false);
                                this.m_cursorPos = this.retrieveWord(this.m_cursorPos, true);
                            }
                            else
                            {
                                // Triple-click(-ish), select everything
                                this.m_selectionStart = 0;
                                this.m_cursorPos = this.m_text.length;
                            }
                        }
                        else
                        {
                            // Normal click, set the cursor where the mouse is
                            this.m_cursorPos = cursorPos;
                            this.m_selectionStart = this.m_cursorPos;
                            this.m_mouseSelecting = true;
                        }

                        this.m_cursorDrawn = true;
                    }
                    else if ((mousePosition.x < position.x || mousePosition.x > position.x + this.m_size.x ||
                                mousePosition.y < position.y || mousePosition.y > position.y + this.m_size.y) &&
                                this.m_focused)
                    {
                        // Click happened outside the textbox
                        this.setFocused(false);

                        this.m_textEventMgr.callHandlers(this, true);
                    }

                    this.m_lastMouseDown = me.timeStamp;
                    break;

                case 'mousemove':
                    if (this.m_focused && this.m_mouseSelecting)
                    {
                        const mousePosition: Vec2D = data;
                        const position: Vec2D = this.getAbsolutePosition();
                        this.m_cursorPos = this.absPositionXToCharacterNbr(mousePosition.x);

                        if ((mousePosition.x < position.x || mousePosition.x > position.x + this.m_size.x) &&
                                this.m_selectionIntervalId === undefined)
                        {
                            // Mouse has moved out of bounds, setup an interval to select the "outside" text
                            this.m_selectionIntervalId = setInterval((incr: number) => {
                                this.m_cursorPos = Math.max(0, Math.min(this.m_text.length, this.m_cursorPos + incr));

                                this.updateTextOffset();
                            }, 50, (mousePosition.x < position.x) ? -1 : 1);
                        }
                        else if (mousePosition.x >= position.x && mousePosition.x <= position.x + this.m_size.x &&
                                    this.m_selectionIntervalId !== undefined)
                        {
                            // Mouse is in bounds again, stop the interval
                            clearInterval(this.m_selectionIntervalId);
                            this.m_selectionIntervalId = undefined;
                        }
                    }

                    break;

                case 'mouseup':
                    // Exit selection mode and clear the interval if set
                    this.m_mouseSelecting = false;

                    if (this.m_selectionIntervalId !== undefined)
                    {
                        clearInterval(this.m_selectionIntervalId);
                        this.m_selectionIntervalId = undefined;
                    }

                    break;

                case 'copy':
                    if (!this.m_focused) break;

                    // Copy the selected text (and prevent the browser to do anything more)
                    (e as ClipboardEvent).clipboardData?.setData('text/plain', this.getSelectedText());
                    e.preventDefault();
                    
                    break;

                case 'cut':
                    if (!this.m_focused) break;
                    
                    // Cut the selected text (and prevent the browser to do anything more)
                    (e as ClipboardEvent).clipboardData?.setData('text/plain', this.getSelectedText());
                    e.preventDefault();

                    this.removeText(0);

                    break;

                case 'paste':
                    if (!this.m_focused) break;
                    
                    // Insert the text in the textbox
                    this.insertText((e as ClipboardEvent).clipboardData?.getData('text/plain'));

                    this.updateTextOffset();

                    break;
            }
        }

        /**
         * Sets the textbox focus flag
         * @param focused The new focus flag
         */
        setFocused(focused: boolean): void
        {
            if (focused && !this.m_focused)
            {
                // The widget becomes focused, setup cursor blinking
                this.m_focused = true;
                this.m_cursorDrawn = true;

                this.m_cursorIntervalId = setInterval(() => {
                    this.m_cursorDrawn = !this.m_cursorDrawn;
                    this.refresh();
                }, 1000);
            }
            else if (!focused && this.m_focused)
            {
                // The widget is no longer focused, remove cursor blinking
                this.m_focused = false;
                this.m_cursorDrawn = false;
                this.m_selectionStart = this.m_cursorPos;

                clearInterval(this.m_cursorIntervalId);
                this.m_cursorIntervalId = undefined;
            }

            this.refresh();
        }

        /**
         * Updates the text offset based on the metrics of the text
         * from the beginning to the cursor position.
         * @param textTilCursorMeasures Text metrics from the beginning of the text to the cursor
         */
        private updateTextOffset(textTilCursorMeasures?: TextMetrics): void
        {
            // Get the TextMetrics of the text until the cursor if not passed in the parameter
            if (textTilCursorMeasures === undefined)
            {
                let localCtx: CanvasRenderingContext2D | null = this.m_canvas.getContext('2d');
                if (localCtx === null)
                    return;

                textTilCursorMeasures = localCtx.measureText(this.m_text.slice(0, this.m_cursorPos));
            }

            // Update the text offset to be in bounds
            if (textTilCursorMeasures.width - this.m_textOffset + (TextBox.s_textboxPadding * 2) >= this.m_size.x)
                this.m_textOffset = textTilCursorMeasures.width + (TextBox.s_textboxPadding * 2) - this.m_size.x;
            else if (textTilCursorMeasures.width - this.m_textOffset + TextBox.s_textboxPadding <= TextBox.s_textboxPadding)
                this.m_textOffset = textTilCursorMeasures.width + TextBox.s_textboxPadding - TextBox.s_textboxPadding;
        }

        /**
         * Computes the position of the character that is on a certain absolute X coordinate.
         * @param posX The absolute X coordinate to look for a character
         */
        private absPositionXToCharacterNbr(posX: number): number
        {
            // Prepare positions
            const position = this.getAbsolutePosition();
            const normPosX: number = posX - position.x + this.m_textOffset - TextBox.s_textboxPadding;
            let i: number = 0;
            
            // Grab the canvas context
            let localCtx: CanvasRenderingContext2D | null = this.m_canvas.getContext('2d');
            if (localCtx !== null)
            {
                // Increment the cursor until we get to the requested position
                let currentWidth: number = 0;
                while ((currentWidth < normPosX || currentWidth < this.m_textOffset) && i < this.m_text.length &&
                        currentWidth - this.m_textOffset < this.m_size.x)
                {
                    currentWidth = localCtx.measureText(this.m_text.slice(0, ++i)).width;
                }

                // Tweak the character number depending on the delta between the requested position and the one we're at
                if (currentWidth - normPosX < localCtx.measureText(this.m_text[i - 1]).width && normPosX < currentWidth)
                    i = Math.max(0, i - 1);
            }

            return i;
        }

        /**
         * Inserts a text where the cursor is,
         * replacing any selected text.
         * @param text Text to insert
         * @param sendEvent Whether to send an event because the text changed
         * @param bypassInputPredicate Whether to bypass the input predicate
         */
        private insertText(text?: string, sendEvent: boolean = true, bypassInputPredicate: boolean = false): void
        {
            if (text === undefined) return;

            // Check against the input predicate if the insertion is authorized
            if (!bypassInputPredicate && this.m_textInputPredicate !== undefined && !this.m_textInputPredicate(text)) return;

            // Remove the selected text if any
            this.removeText(0);

            // Insert the text behind the cursor
            this.m_text = this.m_text.slice(0, this.m_cursorPos) + text + this.m_text.slice(this.m_cursorPos);
            this.m_cursorPos += text.length;
            this.m_selectionStart = this.m_cursorPos;

            // Call handlers if requested
            if (sendEvent)
                this.m_textEventMgr.callHandlers(this, false);

            this.refresh();
        }

        /**
         * Removes an amount of text where the cursor is,
         * ***or*** the selected text if any.
         * @param count Number of characters to remove:
         * - ``count > 0`` will remove ``count`` characters at the right of the cursor.
         * - ``count < 0`` will remove ``count`` characters at the left of the cursor.
         * @param sendEvent Whether to send an event because the text changed
         */
        private removeText(count: number, sendEvent: boolean = true): string
        {
            // Calculate the area that's being removed
            const endCharPosition: number = this.m_cursorPos + count;

            let startSlice: number;
            let endSlice: number;

            // Check if there's a selection
            if (this.m_cursorPos === this.m_selectionStart)
            {
                startSlice = Math.min(this.m_cursorPos, endCharPosition);
                endSlice = Math.max(this.m_cursorPos, endCharPosition);
            }
            else
            {
                startSlice = Math.min(this.m_cursorPos, this.m_selectionStart);
                endSlice = Math.max(this.m_cursorPos, this.m_selectionStart);
            }

            // Remove the text
            const removedText: string = this.m_text.slice(startSlice, endSlice);
            this.m_text = this.m_text.slice(0, startSlice) + this.m_text.slice(endSlice);

            if (this.m_selectionStart < this.m_cursorPos || (count < 0 && this.m_selectionStart === this.m_cursorPos))
                this.m_cursorPos -= removedText.length;

            this.m_selectionStart = this.m_cursorPos;

            // Substract the width of the removed text to the offset
            let localCtx: CanvasRenderingContext2D | null = this.m_canvas.getContext('2d');
            if (localCtx !== null && this.m_textOffset > 0)
                this.m_textOffset = Math.max(0, this.m_textOffset - localCtx.measureText(removedText).width);

            // Call handlers if requested
            if (sendEvent)
                this.m_textEventMgr.callHandlers(this, false);

            this.refresh();

            return removedText;
        }

        /**
         * Retrieves a word from a beginning position.
         * 
         * This implementation will stop whenever it reaches the beginning or the end of the text,
         * or encounters a character that is (not) a letter, depending on the character found
         * in the starting position.
         * @param beginPosition The position to begin from
         * @param leftToRight Flag indicating whether to go to the right or the left
         */
        private retrieveWord(beginPosition: number, leftToRight: boolean = true): number
        {
            // Loop until we encounter a non-alphanumeric character or the start/end of text
            const increment: number = (leftToRight) ? 1 : -1;
            const wordRegex: RegExp = new RegExp(/[A-Za-zÀ-ÖØ-öø-ÿ0-9]/);
            const currentCharMatches: boolean = wordRegex.test(this.m_text[(leftToRight) ? beginPosition : beginPosition + increment]);

            while (((beginPosition > 0 && !leftToRight) || (beginPosition < this.m_text.length && leftToRight)) &&
                wordRegex.test(this.m_text[(leftToRight) ? beginPosition : beginPosition + increment]) === currentCharMatches)
            {
                beginPosition += increment;
            }

            return beginPosition;
        }

        /**
         * Sets the text of the textbox.
         * @param text The new text
         * @param sendEvent Whether to send an event because the text changed
         */
        setText(text: string, sendEvent: boolean = true): void
        {
            this.m_text = text;
            this.m_cursorPos = this.m_text.length;
            this.m_selectionStart = this.m_cursorPos;

            this.updateTextOffset();
            this.refresh();

            if (sendEvent)
                this.m_textEventMgr.callHandlers(this, this.m_text);
        }

        /**
         * Gets the text of the textbox.
         * @returns The text
         */
        getText(): string { return this.m_text; }

        /**
         * Gets the text that is being selected.
         * @returns The selected text
         */
        getSelectedText(): string { return this.m_text.slice(Math.min(this.m_cursorPos, this.m_selectionStart), Math.max(this.m_cursorPos, this.m_selectionStart)); }
    
        /**
         * Adds an handler to the text event manager.
         * 
         * Example:
         * ```TypeScript
         * let textbox: KwimMod.TextBox = new KwimMod.TextBox('textbox', {x: 8, y: 8}, {x: 256, y: 24});
         * 
         * textbox.addTextEventHandler((target: KwimMod.TextBox, finished: boolean) => {
         *     // finished is set to true if the user just quitted the textbox
         *     console.log('Textbox', target, 'has new text, is editing finished?', finished);
         * });
         * ```
         * @param f Handler to add
         */
        addTextEventHandler(f: Function): void { this.m_textEventMgr.addHandler(f); }

        /**
         * Removes an handler to the text event manager.
         * @param f Handler to remove
         */
        removeTextEventHandler(f: Function): void { this.m_textEventMgr.removeHandler(f); }

        /**
         * Sets the input predicate.
         * 
         * This predicate will be called every time {@link insertText} is called,
         * and its return value will be used to determine whether the text is allowed
         * to be inserted.
         * 
         * Example:
         * ```TypeScript
         * let textbox: KwimMod.TextBox = new KwimMod.TextBox('textbox', {x: 8, y: 8}, {x: 284, y: 24});
         * testtxtbx.setInputPredicate((text: string) => {
         *     // Return true only if the entire string is numbers
         *     return new RegExp(/^[0-9]*$/g).test(text);
         * });
         * ```
         * @param f Input predicate
         */
        setInputPredicate(f: Function): void { this.m_textInputPredicate = f; }
    }

    /**
     * Represents a textbox that can only hold numerical values,
     * with buttons to increment and decrement the value.
     */
    export class SpinBox extends Widget
    {
        /** Size of the spinbox */
        private m_size: Vec2D;

        /** The textbox */
        private m_nbrTextbox: TextBox;

        /** The increment button */
        private m_minusButton: Button;

        /** The decrement button */
        private m_plusButton: Button;

        /** The current value */
        private m_value: number;

        /** The minimum value */
        private m_minValue: number;

        /** The maximum value */
        private m_maxValue: number;

        /** The increment to use with the buttons */
        private m_increment: number;

        /** Event manager for when the value changed */
        private m_valueEventMgr: EventHandlerManager;
        
        /** Size of the buttons */
        private static readonly s_buttonSize: number = 24;

        /** Margin between the buttons and the textbox */
        private static readonly s_textboxMargin: number = 8;

        /**
         * Constructs a new spinbox
         * @param id ID of the spinbox
         * @param position Position of the spinbox
         * @param size Size of the spinbox
         */
        constructor(id: string, position: Vec2D, size: Vec2D)
        {
            super(id, position);

            this.m_size = size;

            this.m_value = 0;
            this.m_minValue = 0;
            this.m_maxValue = 100;
            this.m_increment = 1;

            this.m_valueEventMgr = new EventHandlerManager();

            // Setup the textbox
            let nbrTextbox: TextBox = new TextBox('nbrTextbox', {x: SpinBox.s_buttonSize + SpinBox.s_textboxMargin, y: 0},
                                                        {x: this.m_size.x - (SpinBox.s_buttonSize + SpinBox.s_textboxMargin) * 2, y: this.m_size.y});
            this.addChild(nbrTextbox);
            this.m_nbrTextbox = nbrTextbox;

            nbrTextbox.addTextEventHandler((target: TextBox, finished: boolean) => {
                if (!finished) return;

                // Check if the text is a number
                const textAsNbr: number = Number(target.getText());
                if (!Number.isNaN(textAsNbr))
                {
                    // Set the value
                    this.setValue(textAsNbr);
                }
                else
                {
                    // Text is not a number, reset it to the minimum value
                    this.setValue(0);
                }
            });

            nbrTextbox.setInputPredicate((text: string) => {
                // Only allow numbers and decimal points
                return !Number.isNaN(Number(text)) || text == '.';
            });

            nbrTextbox.setText(this.m_value.toString(), false);

            // Setup the buttons
            let minusBtn: Button = new Button('minusBtn', '-', {x: 0, y: 0}, {x: SpinBox.s_buttonSize, y: this.m_size.y});
            let plusBtn: Button = new Button('plusBtn', '+', {x: this.m_size.x - SpinBox.s_buttonSize, y: 0}, {x: SpinBox.s_buttonSize, y: this.m_size.y});

            this.addChild(minusBtn);
            this.addChild(plusBtn);

            this.m_minusButton = minusBtn;
            this.m_plusButton = plusBtn;

            minusBtn.addClickHandler(() => {
                // Decrement the value
                this.setValue(this.m_value - this.m_increment);
            });

            plusBtn.addClickHandler(() => {
                // Increment the value
                this.setValue(this.m_value + this.m_increment);
            });

            minusBtn.setEnabled(false);

            this.m_ready = true;
        }

        /**
         * Sets the value of the spinbox.
         * @param value The new value
         * @param sendEvent Whether to send an event because the value changed
         */
        setValue(value: number, sendEvent: boolean = true)
        {
            // Clamp the value
            const clampedValue: number = Math.max(this.m_minValue, Math.min(value, this.m_maxValue));
            if (clampedValue != this.m_value)
            {
                // Set it if not the same
                this.m_value = clampedValue;
                this.m_nbrTextbox.setText(this.m_value.toString(), false);

                // Send the event
                if (sendEvent)
                    this.m_valueEventMgr.callHandlers(this, this.m_value);

                // Disable buttons if required
                this.m_minusButton.setEnabled(this.m_value !== this.m_minValue);
                this.m_plusButton.setEnabled(this.m_value !== this.m_maxValue);

                this.refresh();
            }
        }

        /**
         * Gets the value of the spinbox.
         * @returns The current value
         */
        getValue(): number { return this.m_value; }

        /**
         * Sets the minimum value of the spinbox.
         * @param minValue The new minimum value
         */
        setMinValue(minValue: number)
        {
            this.m_minValue = minValue;
            this.setValue(Math.max(minValue, this.m_value));
        }

        /**
         * Gets the minimum value of the spinbox.
         * @returns The actual minimum value
         */
        getMinValue(): number { return this.m_minValue; }

        /**
         * Sets the maximum value of the spinbox.
         * @param maxValue The new maximum value
         */
        setMaxValue(maxValue: number)
        {
            this.m_maxValue = maxValue;
            this.setValue(Math.min(maxValue, this.m_value));
        }

        /**
         * Gets the maximum value of the spinbox.
         * @returns The actual maximum value
         */
        getMaxValue(): number { return this.m_maxValue; }

        /**
         * Sets the increment of the spinbox.
         * 
         * This is the value used to add/substract when the
         * plus and minus buttons are clicked.
         * @param increment The new increment
         */
        setIncrement(increment: number) { this.m_increment = Math.abs(increment); }

        /**
         * Gets the increment of the spinbox.
         * @returns The actual increment
         */
        getIncrement(): number { return this.m_increment; }

        /**
         * Adds an handler to the value event manager.
         * 
         * Example:
         * ```TypeScript
         * let spinbox: KwimMod.SpinBox = new KwimMod.SpinBox('spinbox', {x: 8, y: 8}, {x: 256, y: 24});
         * 
         * spinbox.addValueHandler((target: KwimMod.SpinBox, value: number) => {
         *     console.log('Spinbox', target, 'updated its value', value);
         * });
         * ```
         * @param f Handler to add
         */
        addValueHandler(f: Function) { this.m_valueEventMgr.addHandler(f); }

        /**
         * Removes an handler to the value event manager.
         * @param f Handler to remove
         */
        removeValueHandler(f: Function) { this.m_valueEventMgr.removeHandler(f); }
    }

    /**
     * A widget representing a color wheel that the user
     * can choose a color from.
     * 
     * For performance reasons, the color wheel is only generated once
     * the first color picker is instanciated and used for all subsequent
     * rendering of any color picker.
     */
    export class ColorPicker extends Widget
    {
        /** Radius of the color wheel */
        private m_radius: number;

        /** Flag indicating if the color wheel has focus */
        private m_focused: boolean;

        /** Position of the color picker */
        private m_pickerPosition: Vec2D;

        /** Color picked by the color picker */
        private m_pickedColorHsl: Vec3D;

        /** Event manager for when the color changed */
        private m_colorEventMgr: EventHandlerManager;

        /** Radius of the pre-generated color wheel */
        private static readonly s_colorWheelRadius: number = 100;

        /** Canvas containing the pre-generated color wheel */
        private static s_colorWheel?: HTMLCanvasElement;

        /** Lightness value at the border of the color wheel */
        private static readonly s_lightnessBorder: number = 0.5;

        /** Lightness value at the center of the color wheel */
        private static readonly s_lightnessCenter: number = 1;

        /** Difference of lightness between the center and the border */
        private static readonly s_lightnessDelta: number = ColorPicker.s_lightnessCenter - ColorPicker.s_lightnessBorder;

        /**
         * Constructs a new color wheel.
         * @param id ID of the color wheel
         * @param position Position of the color wheel center
         * @param radius Radius of the color wheel
         */
        constructor(id: string, position: Vec2D, radius: number)
        {
            super(id, position);

            this.m_radius = radius;
            this.m_focused = false;

            this.m_pickerPosition = {x: 0, y: 0};
            this.m_pickedColorHsl = {x: 0, y: 1, z: ColorPicker.s_lightnessCenter};

            this.m_colorEventMgr = new EventHandlerManager();

            // Generate the color wheel if not already
            if (ColorPicker.s_colorWheel === undefined)
            {
                // Prepare the canvas context
                ColorPicker.s_colorWheel = document.createElement('canvas');
                ColorPicker.s_colorWheel.height = ColorPicker.s_colorWheelRadius * 2;
                ColorPicker.s_colorWheel.width = ColorPicker.s_colorWheelRadius * 2;

                let ctx: CanvasRenderingContext2D | null = ColorPicker.s_colorWheel.getContext('2d');
                if (ctx !== null)
                {
                    // Generate the color wheel
                    for (let i = 0; i < 360; ++i)
                    {
                        var gradient = ctx.createRadialGradient(ColorPicker.s_colorWheelRadius, ColorPicker.s_colorWheelRadius, 0,
                            ColorPicker.s_colorWheelRadius, ColorPicker.s_colorWheelRadius, ColorPicker.s_colorWheelRadius);

                        gradient.addColorStop(0, 'hsl(' + i + 'deg, 100%, ' + ColorPicker.s_lightnessCenter * 100 + '%)');
                        gradient.addColorStop(1, 'hsl(' + i + 'deg, 100%, ' + ColorPicker.s_lightnessBorder * 100 + '%)');
                        ctx.fillStyle = gradient;

                        ctx.beginPath();
                        ctx.moveTo(ColorPicker.s_colorWheelRadius, ColorPicker.s_colorWheelRadius);
                        ctx.arc(ColorPicker.s_colorWheelRadius, ColorPicker.s_colorWheelRadius,
                            ColorPicker.s_colorWheelRadius, ColorPicker.degreeToRadian(i - 90), ColorPicker.degreeToRadian(i - 88));

                        ctx.fill();
                    }
                }
            }

            this.m_ready = true;
        }

        /**
         * Renders (and rescales) the pre-generated color wheel and the color picker
         * using the provided context.
         * @param ctx Rendering context
         */
        draw(ctx: CanvasRenderingContext2D)
        {
            const drawPosition: Vec2D = this.getAbsolutePosition();
            const wheelCenter: Vec2D = this.getWheelAbsoluteCenter();
            
            // Draw the color wheel to scale
            if (ColorPicker.s_colorWheel !== undefined)
                ctx.drawImage(ColorPicker.s_colorWheel, drawPosition.x, drawPosition.y, this.m_radius * 2, this.m_radius * 2);

            ctx.fillStyle = 'hsl(' + this.m_pickedColorHsl.x + 'deg, ' + this.m_pickedColorHsl.y * 100 + '%, ' + this.m_pickedColorHsl.z * 100 + '%)';
            Widget.drawCircle(ctx, {x: wheelCenter.x + this.m_pickerPosition.x, y: wheelCenter.y + this.m_pickerPosition.y}, 10);

            ctx.strokeStyle = '#6D6D6D';
            ctx.lineWidth = 2;
            ctx.stroke();

            super.draw(ctx);
        }

        /**
         * Checks if a click happened inside the wheel, and sets the color picker accordingly.
         * 
         * If the color wheel has focus, properly set the color picker,
         * following the position of the mouse cursor.
         * @param e Event to process
         * @param data Additionnal data
         */
        processEvent(e: Event, data: any)
        {
            super.processEvent(e, data);

            switch (e.type)
            {
                case 'mousemove':
                    if (!this.m_focused) break;

                case 'mousedown':
                    const mousePosition: Vec2D = data;
                    const wheelCenter: Vec2D = this.getWheelAbsoluteCenter();

                    // Compute the distance between the click and the wheel center
                    const distanceCenterMouse: number = Math.sqrt(Math.pow(mousePosition.x - wheelCenter.x, 2) + Math.pow(mousePosition.y - wheelCenter.y, 2));

                    if (distanceCenterMouse <= this.m_radius || this.m_focused)
                    {
                        // Set the widget focused
                        this.m_focused = true;

                        // Compute where the picker should land
                        const correctedPickerPosition: Vec2D = {x: mousePosition.x - wheelCenter.x, y: mousePosition.y - wheelCenter.y};
                        const pickerAngle: number = Math.atan2(correctedPickerPosition.y, correctedPickerPosition.x);
                        let hypLength: number;
                        if (correctedPickerPosition.y !== 0)
                            hypLength = Math.min(1, (correctedPickerPosition.y / this.m_radius) / Math.sin(pickerAngle));
                        else
                            hypLength = Math.abs(correctedPickerPosition.x / this.m_radius);

                        this.m_pickerPosition = {
                            x: hypLength * this.m_radius * Math.cos(pickerAngle),
                            y: hypLength * this.m_radius * Math.sin(pickerAngle)
                        };

                        // Compute the picked color
                        const pickerLightness: number = ((1 - distanceCenterMouse / this.m_radius) * ColorPicker.s_lightnessDelta) + ColorPicker.s_lightnessBorder
                        this.m_pickedColorHsl = {x: ColorPicker.radianToDegree(pickerAngle) + 90 % 360, y: 1, z: Math.max(ColorPicker.s_lightnessBorder, pickerLightness)};
                    
                        this.refresh();
                    }

                    break;

                case 'mouseup':
                    if (this.m_focused)
                    {
                        this.m_focused = false;
                        this.m_colorEventMgr.callHandlers(this);
                    }

                    break;
            }
        }

        /**
         * Converts an angle in degree to an angle in radians.
         * @param degree The angle in degree
         * @returns The angle in radians
         */
        private static degreeToRadian(degree: number): number { return Math.PI / 180 * degree; }

        /**
         * Converts an angle in radians to an angle in degree.
         * @param radian The angle in radians
         * @returns The angle in degree
         */
        private static radianToDegree(radian: number): number { return radian * 180 / Math.PI; }

        /**
         * Computes the absolute position of the center of the color wheel.
         * @returns The absolute position of the center of the color wheel
         */
        private getWheelAbsoluteCenter(): Vec2D
        {
            const position: Vec2D = this.getAbsolutePosition();
            return {x: position.x + this.m_radius, y: position.y + this.m_radius};
        }

        /**
         * Converts a color in the HSL format to the RGB format.
         * @param h Hue value, in degree (``0 <= h <= 360``)
         * @param s Saturation value (``0 <= s <= 1``)
         * @param l Lightness value (``0 <= l <= 1``)
         */
        private static hslToRgb(h: number, s: number, l: number): Vec3D
        {
            function f(n: number): number
            {
                const k: number = (n + (h / 30)) % 12;
                const a: number = s * Math.min(l, 1 - l);
                
                return l - (a * Math.max(-1, Math.min(k - 3, 9 - k, 1)));
            }

            return {x: f(0), y: f(8), z: f(4)};
        }

        /**
         * Converts a color in the RGB format to the HSL format.
         * @param r Red value (``0 <= r <= 1``)
         * @param g Green value (``0 <= g <= 1``)
         * @param b Blue value (``0 <= b <= 1``)
         */
        private static rgbToHsl(r: number, g: number, b: number): Vec3D
        {
            const xmax: number = Math.max(r, g, b);
            const xmin: number = Math.min(r, g, b);
            const c: number = xmax - xmin;
            const l: number = (xmax + xmin) / 2;
            const v: number = l + (c / 2);

            let h: number = 0;
            if (c === 0)
                h = 0;
            else if (v === r)
                h = 60 * ((g - b) / c);
            else if (v === g)
                h = 60 * (2 + (b - r) / c);
            else if (v === b)
                h = 60 * (4 + (r - g) / c);

            let s: number = 0;
            if (l !== 0 && l !== 1)
                s = (v - l) / Math.min(l, 1 - l);
                
            return {x: h, y: s, z: l};
        }

        /**
         * Returns the picked color, converted to the RGB format.
         * @returns The picked RGB color
         */
        getColorAsRgb(): Vec3D { return ColorPicker.hslToRgb(this.m_pickedColorHsl.x, this.m_pickedColorHsl.y, this.m_pickedColorHsl.z); }

        /**
         * Sets the color picked by the color picker, as an RGB color.
         * @param r Red value (``0 <= r <= 1``)
         * @param g Green value (``0 <= g <= 1``)
         * @param b Blue value (``0 <= b <= 1``)
         * @param sendEvent Whether to send an event because the value changed
         */
        setRgbColor(r: number, g: number, b: number, sendEvent: boolean = true): void
        {
            // Convert the color to HSL and send it to the HSL setter
            const convertedColor: Vec3D = ColorPicker.rgbToHsl(r, g, b);
            this.setHslColor(convertedColor.x, convertedColor.y, convertedColor.z, sendEvent);
        }

        /**
         * Returns the picked color, in the HSL format (as stored).
         * @returns The picked HSL color
         */
        getColorAsHsl(): Vec3D { return this.m_pickedColorHsl; }

        /**
         * Sets the color picked by the color picker, as an HSL color.
         * @param h Hue value, in degree (``0 <= h <= 360``)
         * @param s Saturation value (``0 <= s <= 1``)
         * @param l Lightness value (``0 <= l <= 1``)
         * @param sendEvent Whether to send an event because the value changed
         */
        setHslColor(h: number, s: number, l: number, sendEvent: boolean = true): void
        {
            // Set the picked color
            const oldColor: Vec3D = this.m_pickedColorHsl;
            this.m_pickedColorHsl = {x: h, y: s, z: l};

            // Calculate where to place the picker
            const distance: number = this.m_radius - Math.max(0, (l - ColorPicker.s_lightnessBorder) / ColorPicker.s_lightnessDelta) * this.m_radius;
            const angle: number = ColorPicker.degreeToRadian(h - 90);
            this.m_pickerPosition = {x: distance * Math.cos(angle), y: distance * Math.sin(angle)};

            this.refresh();

            if (oldColor.x !== this.m_pickedColorHsl.x || oldColor.y !== this.m_pickedColorHsl.y || oldColor.z !== this.m_pickedColorHsl.z)
                this.m_colorEventMgr.callHandlers(this);
        }

        /**
         * Adds an handler to the color event manager.
         * 
         * Example:
         * ```TypeScript
         * let colorpick: KwimMod.ColorPicker = new KwimMod.ColorPicker('colorpick', {x: 8, y: 8}, 64);
         * 
         * colorpick.addColorHandler((target: KwimMod.ColorPicker) => {
         *    console.log('Color picker', target, 'updated its color');
         * });
         * ```
         * @param f Handler to add
         */
        addColorHandler(f: Function) { this.m_colorEventMgr.addHandler(f); }

        /**
         * Removes an handler to the color event manager.
         * @param f Handler to remove
         */
        removeColorHandler(f: Function) { this.m_colorEventMgr.removeHandler(f); }
    }

    /**
     * A button that can hold two positions,
     * often used to enable or disable something.
     */
    export class Switch extends BaseCheckBox
    {
        /** The size of the switch */
        private m_size: Vec2D;

        /** The corner radius of the switch */
        private static readonly s_switchCorner: number = 4;

        /** The margin of the thumb inside the switch */
        private static readonly s_switchMargin: number = 4;

        /**
         * Constructs a new switch
         * @param id ID of the switch
         * @param position Position of the switch
         * @param size Size of the switch
         * @param checked Initial state of the switch
         */
        constructor(id: string, position: Vec2D, size: Vec2D, checked: boolean = false)
        {
            super(id, position, checked);

            this.m_size = size;

            this.m_ready = true;
        }

        /**
         * Renders the switch and its thumb using the provided context
         * @param ctx Rendering context
         */
        draw(ctx: CanvasRenderingContext2D)
        {
            const drawPosition: Vec2D = this.getAbsolutePosition();

            // Draw the switch background
            if (this.m_checked)
            {
                let bgColor = ctx.createLinearGradient(drawPosition.x, drawPosition.y, drawPosition.x, drawPosition.y + this.m_size.y);
                bgColor.addColorStop(0, '#00ACFF');
                bgColor.addColorStop(1, '#008CFF');
                ctx.fillStyle = bgColor;
            }
            else
            {
                ctx.fillStyle = '#5D5D5D';
            }

            Widget.drawRoundedRectangle(ctx, drawPosition, this.m_size, Switch.s_switchCorner);

            // Draw the switch thumb
            ctx.fillStyle = '#3D3D3D';

            const thumbSize: Vec2D = {x: this.m_size.x / 1.7 - Switch.s_switchMargin * 2, y: this.m_size.y - Switch.s_switchMargin * 2};
            let thumbPosition: Vec2D = {x: drawPosition.x + Switch.s_switchMargin, y: drawPosition.y + Switch.s_switchMargin};
            if (this.m_checked)
                thumbPosition.x += this.m_size.x - this.m_size.x / 1.7;

            Widget.drawRoundedRectangle(ctx, thumbPosition, thumbSize, Switch.s_switchCorner);

            ctx.fillStyle = '#7D7D7D';
            ctx.fillRect(thumbPosition.x + thumbSize.x / 2, thumbPosition.y + 4, 1, thumbSize.y - 8);
            ctx.fillRect(thumbPosition.x + thumbSize.x / 2 + 4, thumbPosition.y + 4, 1, thumbSize.y - 8);
            ctx.fillRect(thumbPosition.x + thumbSize.x / 2 - 4, thumbPosition.y + 4, 1, thumbSize.y - 8);

            super.draw(ctx);
        }

        /**
         * Checks if a click happened inside the switch,
         * and inverts its state if so.
         * @param e Event to process
         * @param data Additionnal data
         */
        processEvent(e: Event, data: any)
        {
            super.processEvent(e, data);

            if (e.type == 'mouseup')
            {
                // Check if the click happened inside the switch boundaries
                const clickPosition: Vec2D = data;
                const position: Vec2D = this.getAbsolutePosition();
                const switchSize: Vec2D = this.getSize();

                if (clickPosition.x >= position.x && clickPosition.x <= position.x + switchSize.x &&
                    clickPosition.y >= position.y && clickPosition.y <= position.y + switchSize.y)
                    this.setChecked(!this.isChecked());
            }
        }

        /**
         * Sets the size of the switch.
         * @param size The new size
         */
        setSize(size: Vec2D): void { this.m_size = size; this.refresh(); }

        /**
         * Gets the size of the switch.
         * @returns The size
         */
        getSize(): Vec2D { return this.m_size; }
    }

    /** Stores the currently opened windows */
    let m_windows: Array<Window> = [];

    /** Stored the position of the grabbed window */
    let m_grabbedWindow: number | undefined;

    /** Stores the offset between the mouse position and the top-left corner of the grabbed window */
    let m_grabbedWindowOffset: Vec2D = {x: 0, y: 0};

    /** Event handler for when Kwim needs the canvas to be refreshed */
    let m_resfreshEventMgr: EventHandlerManager = new EventHandlerManager();

    /**
     * Adds a new window to the manager.
     * 
     * Note that the window must have a *unique* ID.
     * @param window The window to add
     * @returns ``true`` if the window has been added, ``false`` otherwise
     */
    export function addWindow(window: Window): boolean
    {
        // Check if the window already exists
        for (let curWindow of m_windows)
            if (curWindow.getId() === window.getId())
                return false;

        // Set the currently active window inactive
        m_windows[m_windows.length - 1]?.setFocused(false);

        // Push the window to the main array
        window.setFocused(true);
        m_windows.push(window);
        window.markReady();

        return true;
    }

    /**
     * Closes an opened window.
     * @param id ID of the window to close
     * @param forceClose Whether to force the window to close
     * @returns ``true`` if the window has been closed, ``false`` otherwise
     */
    export function closeWindow(id: string, forceClose: boolean = false): boolean
    {
        let i: number = 0;
        for (; i < m_windows.length; ++i)
        {
            const currentWindow: Window = m_windows[i];
            if (currentWindow.getId() === id)
            {
                if (currentWindow.prepareForClose() || forceClose)
                {
                    m_windows.splice(i, 1);

                    requestRefresh();
                    return true;
                }

                return false;
            }
        }

        return false;
    }

    export function runner(g: Graph): void
    {
        const legalStuff: string = 'Kwim - Kuruyia\'s Widget and Window Manager\n\
\n\
MIT License\n\
\n\
Copyright (c) 2020 Alexandre "Kuruyia" SOLLIER\n\
\n\
Permission is hereby granted, free of charge, to any person obtaining a copy\n\
of this software and associated documentation files (the "Software"), to deal\n\
in the Software without restriction, including without limitation the rights\n\
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell\n\
copies of the Software, and to permit persons to whom the Software is\n\
furnished to do so, subject to the following conditions:\n\
\n\
The above copyright notice and this permission notice shall be included in all\n\
copies or substantial portions of the Software.\n\
\n\
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\n\
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\n\
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\n\
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\n\
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\n\
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\n\
SOFTWARE.';

        let container: HTMLDivElement = document.createElement('div');

        let i = -1;
        let oldI = 0;
        while ((i = legalStuff.indexOf('\n', i + 1)) !== -1)
        {
            let p: HTMLParagraphElement = document.createElement('p');
            const currentLine: string = legalStuff.slice(oldI, i);
            p.innerHTML = (currentLine.trim().length > 0) ? currentLine : '&#8205;';
            container.appendChild(p);

            oldI = i;
        }

        let p: HTMLParagraphElement = document.createElement('p');
        p.innerHTML = legalStuff.slice(oldI, legalStuff.length);
        container.appendChild(p);

        interfaceOutputHTML(container);
    }

    export function displayRunner(ctx: CanvasRenderingContext2D): void
    {
        // Draw every window
        for (let window of m_windows)
            window.draw(ctx);
    }

    /**
     * Handles any mouse event.
     * 
     * This must be setup as follows:
     * ```TypeScript
     * canvas.addEventListener('mousedown', KwimMod.mouseHandler);
     * canvas.addEventListener('mousemove', KwimMod.mouseHandler);
     * canvas.addEventListener('mouseup', KwimMod.mouseHandler);
     * ```
     * @param this 
     * @param e The mouse event
     */
    export function mouseHandler(this: any, e: MouseEvent): void
    {
        // Compute the mouse coordinates inside the canvas
        const canvasBoundingRect: DOMRect = this.getBoundingClientRect();
        const mousePosition: Vec2D = {x: e.clientX - canvasBoundingRect.x, y: e.clientY - canvasBoundingRect.y};
        let eventData: any = mousePosition;
        
        switch (e.type)
        {
            case 'mousedown':
                // Set the currently focused window unfocused
                m_windows[m_windows.length - 1]?.setFocused(false);

                // Check if we have a window under the cursor, and set it focused
                let i: number = m_windows.length;
                while (i-- > 0)
                {
                    const currentWindow: Window = m_windows[i];
                    const currentWindowAbsolutePosition = currentWindow.getAbsolutePosition();
                    const currentWindowPosition = currentWindow.getPosition();
                    const currentWindowSize = currentWindow.getSize();

                    if (mousePosition.x >= currentWindowAbsolutePosition.x && mousePosition.x <= currentWindowAbsolutePosition.x + currentWindowSize.x &&
                        mousePosition.y >= currentWindowAbsolutePosition.y && mousePosition.y <= currentWindowAbsolutePosition.y + currentWindowSize.y)
                    {
                        // Set the selected window focused
                        currentWindow.setFocused(true);

                        if (currentWindow.isPositionOnTitlebar(mousePosition))
                        {
                            m_grabbedWindow = m_windows.length - 1;
                            m_grabbedWindowOffset = {x: mousePosition.x - currentWindowPosition.x, y: mousePosition.y - currentWindowPosition.y};
                        }

                        // Push the selected window to the back
                        m_windows.push(m_windows.splice(i, 1)[0]);

                        break;
                    }
                }

                break;

            case 'mousemove':
                // Move the window if we grabbed one
                if (m_grabbedWindow !== undefined && m_grabbedWindow < m_windows.length)
                {
                    m_windows[m_grabbedWindow].setPosition({x: mousePosition.x - m_grabbedWindowOffset.x, y: mousePosition.y - m_grabbedWindowOffset.y});
                    requestRefresh();
                }

                break;

            case 'mouseup':
                // Release the grabbed window
                m_grabbedWindow = undefined;

                break;
        }

        // Send the event to every window
        for (let window of m_windows)
            window.processEvent(e, eventData);
    }

    /**
     * Handles any required event.
     * 
     * This must be setup as follows:
     * ```TypeScript
     * window.addEventListener('keydown', KwimMod.genericHandler);
     * window.addEventListener('keypress', KwimMod.genericHandler);
     * window.addEventListener('keyup', KwimMod.genericHandler);
     * 
     * window.addEventListener('copy', KwimMod.genericHandler);
     * window.addEventListener('cut', KwimMod.genericHandler);
     * window.addEventListener('paste', KwimMod.genericHandler);
     * ```
     * @param this 
     * @param e The event
     */
    export function genericHandler(this: any, e: Event): void
    {
        // Send the event to every window
        for (let window of m_windows)
            window.processEvent(e, undefined);
    }

    /**
     * Searches a window by its ID.
     * @param id ID of the window to look for
     * @returns The window, if found
     */
    export function findWindowById(id: string): Window | undefined
    {
        // Search for the window and return it if found
        for (let window of m_windows)
            if (window.getId() === id)
                return window;

        return undefined;
    }

    /**
     * Requests a refresh to the handlers.
     */
    function requestRefresh(): void { m_resfreshEventMgr.callHandlers(); }

    /**
     * Adds an handler to the refresh event manager.
     * 
     * Example:
     * ```TypeScript
     * KwimMod.addRefreshHandler(refresh);
     * ```
     * @param f Handler to add
     */
    export function addRefreshHandler(f: Function) { m_resfreshEventMgr.addHandler(f); }

    /**
     * Removes an handler to the refresh event manager.
     * @param f Handler to remove
     */
    export function removeRefreshHandler(f: Function) { m_resfreshEventMgr.removeHandler(f); }
}

// Setup graphes-tools to use Kwim
addModRunner(KwimMod.runner, 'Kwim');
addModDisplay(KwimMod.displayRunner);
KwimMod.addRefreshHandler(refresh);

cv.addEventListener('mousedown', KwimMod.mouseHandler);
cv.addEventListener('mousemove', KwimMod.mouseHandler);
cv.addEventListener('mouseup', KwimMod.mouseHandler);

window.addEventListener('keydown', KwimMod.genericHandler);
window.addEventListener('keypress', KwimMod.genericHandler);
window.addEventListener('keyup', KwimMod.genericHandler);

window.addEventListener('copy', KwimMod.genericHandler);
window.addEventListener('cut', KwimMod.genericHandler);
window.addEventListener('paste', KwimMod.genericHandler);