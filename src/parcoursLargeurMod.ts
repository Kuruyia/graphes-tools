module ParcourLargeurMod {

    let VertexInputLabel = document.createElement("label")
    VertexInputLabel.htmlFor = "VertexInputParcourLargeur"
    VertexInputLabel.textContent = "start vertex name : "
    let VertexInput = document.createElement("input")
    VertexInput.id = "VertexInputParcourLargeur"
    let inputEl = document.createElement("p")
    let button = document.createElement("button")
    button.innerText = "Valider"
    button.addEventListener("click", validateStartVertex)
    inputEl.appendChild(VertexInputLabel)
    inputEl.appendChild(VertexInput)
    inputEl.appendChild(button)
    let globalGraph: Graph
    function askStartVertex() {

        interfaceOutputHTML(inputEl)
    }
    function validateStartVertex() {
        let val = VertexInput.value
        let vertexS = g.findVertexByName(val)
        if (vertexS) {
            startAlgo(vertexS)
        } else {
            VertexInputLabel.textContent = "vertex not found, vertex name : "
            askStartVertex()
        }

    }
    function startAlgo(s: Vertex) {
        let elOut = document.createElement("div")
        let L: Array<Vertex> = [s]
        let P: Map<Vertex, Vertex> = new Map()
        let marquedVertices: Array<Vertex> = [s]

        while (L.length > 0) {
            let names: Array<string> = []

            for (let j = 0; j < L.length; ++j) {
                names.push(L[j].nom)
            }
            console.log(`L : ${JSON.stringify(names)}`)
            let x = L.shift()
            if (x) {
                let adj = x.getAdjacent()
                let predecessors: Array<Vertex> = []
                adj.forEach((value, v) => {
                    if (value > 0) {
                        predecessors.push(v)
                    }
                })
                for (let i = 0; i < predecessors.length; ++i) {
                    if (marquedVertices.lastIndexOf(predecessors[i]) == -1) {
                        marquedVertices.push(predecessors[i])
                        P.set(predecessors[i], x)
                        L.push(predecessors[i])
                        let names: Array<string> = []
                        for (let j = 0; j < L.length; ++j) {
                            names.push(L[j].nom)
                        }
                        console.log(`L : ${JSON.stringify(names)}`)
                    }
                }
            }
        }
        let result = document.createElement("p")
        let arbre = `P(${s.nom}): ∅ |`
        P.forEach((value, key) => {
            arbre = arbre + ` P(${key.nom}): ${value.nom} |`
        })

        result.innerHTML = "resultat : " + arbre
        elOut.appendChild(result)
        interfaceOutputHTML(elOut)

    }

    export function runner(g: Graph) {

        globalGraph = g
        askStartVertex()

    }


}
addModRunner(ParcourLargeurMod.runner, "parcours en largeur")